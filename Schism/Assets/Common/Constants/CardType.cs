﻿namespace Assets.Common.Constants
{
    public enum CardType
    {
        DefaultUnit = 0,
        DefaultUnits = 1,
        BruteUnit = 2,
        AoeUnit = 3,
        SwarmUnit = 4,

        ReplaceBruteUnit = 5,
        ReplaceAoeUnit = 6,
        ReplaceSwarmUnit = 7,

        UpgradeBarrack = 8,

        Hellfire = 9,

        GruntUnit = 10,
        ReplaceGruntUnit = 11,
        HeroUnit = 12,

        ArcherUnit = 13,
        ReplaceArcherUnit = 14
    }
}
