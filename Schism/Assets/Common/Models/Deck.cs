﻿using System;
using System.Collections.Generic;
using Assets.Common.Constants;

namespace Assets.Common.Models
{
    [Serializable]
    public class Deck
    {
        public int Id;
        public string Title;
        public List<CardType> Cards;
    }
}
