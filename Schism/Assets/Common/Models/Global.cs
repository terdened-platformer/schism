﻿using System;
using System.Collections.Generic;
using System.IO;
using Assets.Common.Constants;
using UnityEngine;

namespace Assets.Common.Models
{
    [Serializable]
    public class DecksContainer
    {
        public List<Deck> Decks;
    }

    public static class Global
    {
        private static readonly string DecksSavePath;

        public static bool IsServer = true;

        public static bool IsDebug = true;

        public static string Address = "127.0.0.1";

        public static string Port = "7777";

        public static bool UseMlApi = true;

        public static DecksContainer Decks;

        public static Deck SelectedDeck;

        static Global()
        {
            DecksSavePath = Application.persistentDataPath + "/decks.json";
            LoadDecks();
        }

        public static void LoadDecks()
        {
            if (File.Exists(DecksSavePath))
            {
                Decks = JsonUtility.FromJson<DecksContainer>(File.ReadAllText(DecksSavePath));
            } else
            {
                InitDecks();
            }
        }

        public static void SaveDecks()
        {
            var jsonData = JsonUtility.ToJson(Decks);
            File.WriteAllText(DecksSavePath, jsonData);
        }

        private static void InitDecks()
        {
            Decks = new DecksContainer {
                Decks = new List<Deck>
                {
                    new Deck {
                        Id = 1,
                        Title = "Deck #1",
                        Cards = new List<CardType>
                        {
                            CardType.AoeUnit,
                            CardType.BruteUnit,
                            CardType.DefaultUnit,
                            CardType.DefaultUnits,
                            CardType.Hellfire,
                            CardType.ReplaceAoeUnit,
                            CardType.ReplaceBruteUnit,
                            CardType.ReplaceSwarmUnit,
                            CardType.SwarmUnit,
                            CardType.UpgradeBarrack
                        }
                    }
                }
            };
        }
    }
}
