﻿using System;
using UnityEngine;

namespace Assets.Common.Utils
{
    public class AnimationEventEmitter : MonoBehaviour
    {
        public event Action Rise;

        public void OnEvent()
        {
            Rise?.Invoke();
        }
    }
}
