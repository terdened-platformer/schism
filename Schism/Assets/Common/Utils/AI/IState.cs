﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Common.Utils.AI
{
    public interface IState
    {
        [SerializeField]
        List<BaseTransition> Transitions { get; set; }

        [SerializeField]
        string Name { get; }

        void HandleState();

        void OnEnter();

        void OnExit();
    }
}
