﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Common.Utils.AI
{
    public class ColliderTrigger : MonoBehaviour
    {
        [SerializeField]
        public delegate void TriggerHandler(Collider col);

        [SerializeField]
        public event TriggerHandler OnEnterTriggered;

        [SerializeField]
        public event TriggerHandler OnExitTriggered;

        [SerializeField]
        public List<Collider> TriggerList = new List<Collider>();

        private void OnTriggerEnter(Collider col)
        {
            TriggerList.Add(col);
            OnEnterTriggered?.Invoke(col);
        }

        private void OnTriggerExit(Collider col)
        {
            TriggerList.Remove(col);
            OnExitTriggered?.Invoke(col);
        }
    }
}
