﻿using System;
using UnityEngine;

namespace Assets.Common.Utils.AI
{
    public abstract class BaseTransition
    {
        [SerializeField]
        protected GameObject Self { get; set; }

        [SerializeField]
        protected virtual string TargetState => "Not Defined";

        protected BaseTransition(GameObject self)
        {
            Self = self;
        }

        public string Check()
        {
            return IsTriggered() ? TargetState : null;
        }

        protected abstract bool IsTriggered();

        public virtual void Clear()
        {
            throw new NotImplementedException();
        }
    }
}
