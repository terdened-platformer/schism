﻿using System;
using UnityEngine;

namespace Assets.Common.Utils
{
    public class OnDestroyDispatcher : MonoBehaviour
    {
        public event Action<GameObject> OnObjectDestroyed;

        private void OnDestroy()
        {
            OnObjectDestroyed?.Invoke(gameObject);
        }
    }
}
