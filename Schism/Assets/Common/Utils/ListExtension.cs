﻿using System;
using System.Collections.Generic;

namespace Assets.Common.Utils
{
    public static class ListExtension
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            var counter = list.Count;

            while (counter > 1)
            {
                counter--;
                var rndIndex = new Random().Next(counter + 1);
                (list[rndIndex], list[counter]) = (list[counter], list[rndIndex]);
            }
        }
    }
}
