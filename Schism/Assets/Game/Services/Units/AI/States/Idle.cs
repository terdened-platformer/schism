﻿using System.Collections.Generic;
using Assets.Common.Utils.AI;
using Assets.Game.Controllers.Units;

namespace Assets.Game.Services.Units.AI.States
{
    public class Idle : IState
    {
        public List<BaseTransition> Transitions { get; set; }
        public string Name => "Idle";

        private readonly SimpleUnitController _controller;

        public Idle(SimpleUnitController controller)
        {
            Transitions = new List<BaseTransition>();
            _controller = controller;
        }

        public void HandleState() 
        {
        }

        public void OnEnter()
        {
            _controller.StopMovement();
        }

        public void OnExit()
        {
        }
    }
}
