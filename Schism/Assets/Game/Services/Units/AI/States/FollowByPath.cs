﻿using System.Collections.Generic;
using System.Linq;
using Assets.Common.Utils.AI;
using Assets.Game.Controllers.Paths;
using Assets.Game.Controllers.Units;
using Assets.Game.Models.Paths;
using Assets.Game.Services.Units.AI.Transitions;
using UnityEngine;

namespace Assets.Game.Services.Units.AI.States
{
    public class FollowByPath : IState
    {
        public List<BaseTransition> Transitions { get; set; }
        public string Name => "FollowByPath";

        private readonly SimpleUnitAI _ai;
        private readonly SimpleUnitController _controller;
        private readonly FollowByPathToFollowByPath _followByPathToFollowByPath;
        private readonly List<GameObject> _paths;

        public FollowByPath(GameObject self, SimpleUnitController controller)
        {
            Transitions = new List<BaseTransition>();


            Transitions.Add(new AnyToIdle(self));
            _followByPathToFollowByPath = new FollowByPathToFollowByPath(self);
            Transitions.Add(_followByPathToFollowByPath);

            Transitions.Add(new AnyToMoveToEnemy(self));

            _ai = self.GetComponent<SimpleUnitAI>();
            _controller = controller;

            _paths = GameObject.FindGameObjectsWithTag("Path")
                .Where(_ => _.GetComponent<PathController>().Owner == controller.Owner).ToList();
        }

        public void HandleState()
        {
        }

        public void OnEnter()
        {
            if (_ai.CurrentPathIndex == 0)
            {
                var nearestBarrack = _paths.OrderBy(_ => Vector3.Distance(_.transform.position, _ai.transform.position)).FirstOrDefault();

                if (nearestBarrack != null)
                {
                    _ai.Path = nearestBarrack.GetComponent<PathController>().Path.GetComponent<Path>();
                }
            }

            _followByPathToFollowByPath.Init();
            if (_ai.Path == null || _ai.CurrentPathIndex >= _ai.Path.Checkpoints.Count)
                return;

            _controller.MoveTo(_ai.Path.Checkpoints[_ai.CurrentPathIndex].transform.position);
        }

        public void OnExit()
        {
            _followByPathToFollowByPath.Clear();
        }
    }
}
