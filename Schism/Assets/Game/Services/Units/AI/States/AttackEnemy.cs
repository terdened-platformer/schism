﻿using System.Collections.Generic;
using Assets.Common.Utils.AI;
using Assets.Game.Controllers.Common;
using Assets.Game.Controllers.Units;
using Assets.Game.Services.Units.AI.Transitions;
using UnityEngine;

namespace Assets.Game.Services.Units.AI.States
{
    public class AttackEnemy : IState
    {
        public List<BaseTransition> Transitions { get; set; }
        public string Name => "AttackEnemy";

        private float _counter;
        private readonly SimpleUnitController _controller;
        private readonly SimpleUnitAI _ai;
        private readonly AttackEnemyToMoveToEnemy _attackEnemyToMoveToEnemy;
        private readonly AnyToMoveToEnemy _anyToMoveToEnemy;


        public AttackEnemy(GameObject self, SimpleUnitController controller)
        {
            Transitions = new List<BaseTransition>();

            _attackEnemyToMoveToEnemy = new AttackEnemyToMoveToEnemy(self);
            Transitions.Add(_attackEnemyToMoveToEnemy);

            _anyToMoveToEnemy = new AnyToMoveToEnemy(self);
            Transitions.Add(_anyToMoveToEnemy);

            Transitions.Add(new AnyToMoveToFollowByPath(self));

            _controller = controller;
            _ai = self.GetComponent<SimpleUnitAI>();
        }

        public void HandleState()
        {
            _counter -= Time.deltaTime;
            _controller.LookAtEnemy(_ai.Enemy);

            if (!(_counter <= 0)) return;
            _counter = _ai.Stats.AttackRate;

            if (_ai.Enemy == null) return;

            var enemyEndurance = _ai.Enemy.GetComponent<HealthPower>();
            _controller.Attack(enemyEndurance, _ai.Stats.IsAoe, _ai.Stats.AoeDistance);
        }

        public void OnEnter()
        {
            _controller.StopMovement();
            _attackEnemyToMoveToEnemy.Init();
            _anyToMoveToEnemy.Init();
        }

        public void OnExit()
        {
            _attackEnemyToMoveToEnemy.Clear();
            _anyToMoveToEnemy.Clear();
        }
    }
}
