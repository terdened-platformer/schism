﻿using System.Collections.Generic;
using Assets.Common.Utils.AI;
using Assets.Game.Controllers.Units;
using Assets.Game.Services.Units.AI.Transitions;
using UnityEngine;

namespace Assets.Game.Services.Units.AI.States
{
    public class MoveToEnemy : IState
    {
        public List<BaseTransition> Transitions { get; set; }
        public string Name => "MoveToEnemy";

        private readonly SimpleUnitController _controller;
        private readonly SimpleUnitAI _ai;
        private readonly MoveToEnemyToAttackEnemy _moveToEnemyToAttackEnemy;

        public MoveToEnemy(GameObject self, SimpleUnitController controller)
        {
            Transitions = new List<BaseTransition>();

            _moveToEnemyToAttackEnemy = new MoveToEnemyToAttackEnemy(self);
            Transitions.Add(_moveToEnemyToAttackEnemy);

            Transitions.Add(new AnyToMoveToFollowByPath(self));

            _controller = controller;
            _ai = self.GetComponent<SimpleUnitAI>();
        }

        public void HandleState()
        {
            if (_ai.Enemy != null)
                _controller.MoveTo(_ai.Enemy.transform.position);
        }

        public void OnEnter()
        {
            _moveToEnemyToAttackEnemy.Init();
        }

        public void OnExit()
        {
            _moveToEnemyToAttackEnemy.Clear();
        }
    }
}
