﻿using System.Linq;
using Assets.Common.Utils.AI;
using UnityEngine;

namespace Assets.Game.Services.Units.AI.Transitions
{
    public class MoveToEnemyToAttackEnemy : BaseTransition
    {
        private readonly ColliderTrigger _colliderTrigger;
        private readonly SimpleUnitAI _ai;
        
        protected override string TargetState => "AttackEnemy";

        public MoveToEnemyToAttackEnemy(GameObject self) : base(self)
        {
            _colliderTrigger = self.transform.Find("MeleeCollider").GetComponent<ColliderTrigger>();
            _ai = self.GetComponent<SimpleUnitAI>();

            if (_colliderTrigger == null)
            {
                Debug.LogError("There is no ColliderTrigger");
                return;
            }

            if (_ai != null) return;

            Debug.LogError("There is no SimpleUnitAI");
            return;
        }

        protected override bool IsTriggered()
        {
            return _colliderTrigger.TriggerList.Any(_ =>
            {
                if (_ == null || _.gameObject == null || !_.gameObject.activeInHierarchy)
                    return false;

                return _.gameObject == _ai.Enemy;
            });
        }

        private void OnTriggeredHandler(Collider col)
        {
            //if (col.gameObject == _ai.Enemy)
            //    _isTriggered = true;
        }

        public void Init()
        {
            _colliderTrigger.OnEnterTriggered += OnTriggeredHandler;
        }

        public override void Clear()
        {
            if(_colliderTrigger != null)
                _colliderTrigger.OnEnterTriggered -= OnTriggeredHandler;
        }
    }
}
