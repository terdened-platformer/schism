﻿using Assets.Common.Utils.AI;
using UnityEngine;

namespace Assets.Game.Services.Units.AI.Transitions
{
    public class AnyToIdle : BaseTransition
    {
        protected override string TargetState => "Idle";
        private readonly SimpleUnitAI _ai;

        public AnyToIdle(GameObject self) : base(self)
        {
            _ai = self.GetComponent<SimpleUnitAI>();
        }

        protected override bool IsTriggered()
        {
            return _ai.Path == null || _ai.CurrentPathIndex >= _ai.Path.Checkpoints.Count;
        }
    }
}
