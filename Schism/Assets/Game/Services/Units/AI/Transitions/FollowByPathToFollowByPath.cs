﻿using System.Linq;
using Assets.Common.Utils.AI;
using UnityEngine;

namespace Assets.Game.Services.Units.AI.Transitions
{
    public class FollowByPathToFollowByPath : BaseTransition
    {
        private ColliderTrigger _colliderTrigger;
        private readonly SimpleUnitAI _ai;
        private readonly GameObject _self;
        
        protected override string TargetState => "FollowByPath";

        public FollowByPathToFollowByPath(GameObject self) : base(self)
        {
            _self = self;
            _ai = self.GetComponent<SimpleUnitAI>();
        }

        protected override bool IsTriggered()
        {
            if (_colliderTrigger.TriggerList.Where(_ => _ != null).All(_ => _.gameObject != _self))
                return false;

            _ai.CurrentPathIndex++;
            Clear();
            Init();

            return true;
        }

        private void OnTriggeredHandler(Collider col)
        {
        }

        public void Init()
        {
            if (_ai.Path.Checkpoints.Count <= _ai.CurrentPathIndex)
                return;

            _colliderTrigger = _ai.Path.Checkpoints[_ai.CurrentPathIndex].GetComponent<ColliderTrigger>();
            _colliderTrigger.OnEnterTriggered += OnTriggeredHandler;
        }

        public override void Clear()
        {
            if(_colliderTrigger != null)
                _colliderTrigger.OnEnterTriggered -= OnTriggeredHandler;
        }
    }
}
