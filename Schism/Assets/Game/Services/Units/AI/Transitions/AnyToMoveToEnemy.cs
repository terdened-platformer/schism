﻿using System.Linq;
using Assets.Common.Utils.AI;
using Assets.Game.Controllers.Buildings;
using Assets.Game.Controllers.Common;
using UnityEngine;

namespace Assets.Game.Services.Units.AI.Transitions
{
    public class AnyToMoveToEnemy : BaseTransition
    {
        private readonly ColliderTrigger _colliderTrigger;
        private readonly SimpleUnitAI _ai;

        [SerializeField]
        protected override string TargetState => "MoveToEnemy";

        public AnyToMoveToEnemy(GameObject self) : base(self)
        {
            _colliderTrigger = self.transform.Find("VisionCollider").GetComponent<ColliderTrigger>();
            _ai = self.GetComponent<SimpleUnitAI>();

            if (_colliderTrigger == null)
            {
                Debug.LogError("There is no ColliderTrigger");
                return;
            }

            if (_ai != null) return;

            Debug.LogError("There is no SimpleUnitAI");
            return;
        }

        protected override bool IsTriggered()
        {
            var enemies = _colliderTrigger.TriggerList.Where(_ =>
            {
                if (_ == null || _.gameObject == null || !_.gameObject.activeInHierarchy)
                    return false;

                var endurance = _.gameObject.GetComponent<HealthPower>();
                var owner = _.gameObject.GetComponent<SimpleUnitAI>() != null ? _.gameObject.GetComponent<SimpleUnitAI>().Owner : _.gameObject.GetComponent<Barrack>()?.Owner;

                return owner != null && owner != _ai.Owner && (!endurance.IsFly || (endurance.IsFly && _ai.Stats.CanAttackFly));
            }).ToList();

            if (!enemies.Any()) return false;

            enemies = enemies.OrderByDescending(_ => GetEnemyTaunt(_.GetComponent<HealthPower>())).ToList();

            if (_ai.Enemy == null)
            {
                _ai.Enemy = enemies.First().gameObject;
                return true;
            }
            else
            {
                var currentTaunt = GetEnemyTaunt(_ai.Enemy.GetComponent<HealthPower>());

                if (currentTaunt < GetEnemyTaunt(enemies.First().GetComponent<HealthPower>()) && _ai.Enemy != enemies.First())
                {
                    _ai.Enemy = enemies.First().gameObject;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private float GetEnemyTaunt(HealthPower enduranceScript)
        {
            var distance = Vector3.Distance(_ai.transform.position, enduranceScript.transform.position);
            return distance > 0 ? enduranceScript.Taunt / distance : 100;
        }

        private void OnTriggeredHandler(Collider col)
        {
        }

        public void Init()
        {
            _colliderTrigger.OnEnterTriggered += OnTriggeredHandler;
        }

        public override void Clear()
        {
            if(_colliderTrigger != null)
                _colliderTrigger.OnEnterTriggered -= OnTriggeredHandler;
        }
    }
}
