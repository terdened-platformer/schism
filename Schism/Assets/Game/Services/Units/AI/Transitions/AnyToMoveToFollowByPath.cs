﻿using Assets.Common.Utils.AI;
using UnityEngine;

namespace Assets.Game.Services.Units.AI.Transitions
{
    public class AnyToMoveToFollowByPath : BaseTransition
    {
        private readonly SimpleUnitAI _ai;

        protected override string TargetState => "FollowByPath";

        public AnyToMoveToFollowByPath(GameObject self) : base(self)
        {
            _ai = self.GetComponent<SimpleUnitAI>();

            if (_ai != null) return;

            Debug.LogError("There is no SimpleUnitAI");
            return;
        }

        protected override bool IsTriggered()
        {
            return _ai.Enemy == null || !_ai.Enemy.activeInHierarchy;
        }
    }
}
