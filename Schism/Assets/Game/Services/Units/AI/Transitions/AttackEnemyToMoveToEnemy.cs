﻿using Assets.Common.Utils.AI;
using UnityEngine;

namespace Assets.Game.Services.Units.AI.Transitions
{
    public class AttackEnemyToMoveToEnemy : BaseTransition
    {
        private bool _isTriggered;
        private readonly ColliderTrigger _colliderTrigger;
        private readonly SimpleUnitAI _ai;

        protected override string TargetState => "MoveToEnemy";

        public AttackEnemyToMoveToEnemy(GameObject self) : base(self)
        {
            _colliderTrigger = self.transform.Find("MeleeCollider").GetComponent<ColliderTrigger>();
            _ai = self.GetComponent<SimpleUnitAI>();

            if (_colliderTrigger == null)
            {
                Debug.LogError("There is no ColliderTrigger");
                return;
            }

            if (_ai != null) return;

            Debug.LogError("There is no SimpleUnitAI");
            return;
        }

        protected override bool IsTriggered()
        {
            return _isTriggered;
        }

        private void OnTriggeredHandler(Collider col)
        {
            if (col.gameObject == _ai.Enemy)
                _isTriggered = true;
        }

        public void Init()
        {
            _isTriggered = false;
            _colliderTrigger.OnExitTriggered += OnTriggeredHandler;
        }

        public override void Clear()
        {
            _isTriggered = false;

            if (_colliderTrigger != null)
                _colliderTrigger.OnEnterTriggered -= OnTriggeredHandler;
        }
    }
}
