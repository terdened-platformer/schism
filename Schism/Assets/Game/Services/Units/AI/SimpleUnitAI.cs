﻿using Assets.Common.Constants;
using Assets.Common.Utils.AI;
using Assets.Game.Controllers.Units;
using Assets.Game.Models.Paths;
using Assets.Game.Models.Units;
using Assets.Game.Services.Units.AI.States;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Game.Services.Units.AI
{
    [RequireComponent(typeof(SimpleUnitController))]
    public class SimpleUnitAI : NetworkBehaviour
    {
        public int CurrentPathIndex = 0;
        public UnitStats Stats;
        public PlayerType Owner;
        public Path Path;
        public GameObject TargetBarrack = null;
        public GameObject Enemy = null;

        private StateMachineAI _stateMachineAI;
        private NavMeshAgent _navMeshAgent;

        public void Start()
        {
            Stats = GetComponent<UnitStats>();
        }

        public void Init()
        {
            var simpleUnitController = GetComponent<SimpleUnitController>();

            _stateMachineAI = new StateMachineAI();

            var followByPath = new FollowByPath(transform.gameObject, simpleUnitController);
            var moveToEnemy = new MoveToEnemy(transform.gameObject, simpleUnitController);
            var attackEnemy = new AttackEnemy(transform.gameObject, simpleUnitController);
            var idle = new Idle(simpleUnitController);

            _stateMachineAI.States.Add(followByPath);
            _stateMachineAI.States.Add(moveToEnemy);
            _stateMachineAI.States.Add(attackEnemy);
            _stateMachineAI.States.Add(idle);

            _stateMachineAI.SetCurrentState("FollowByPath");
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public void Update()
        {
            if (!NetworkManager.Singleton.IsServer)
                return;

            if (_stateMachineAI == null)
                return;

            _stateMachineAI.Update();
        }
    }
}
