﻿using Assets.Common.Constants;

namespace Assets.Game.Services.Players
{
    public interface IPlayerStateService
    {
        int GetMoney(PlayerType player);

        void AddMoney(PlayerType player, int amount);

        void SpendMoney(PlayerType player, int amount);

        bool CanSpendMoney(PlayerType player, int amount);
    }
}