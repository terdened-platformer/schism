﻿using System;
using Assets.Common.Constants;
using Assets.Game.Models.Player;
using Assets.Game.Repositories.Players;

namespace Assets.Game.Services.Players
{
    public class PlayerStateService: IPlayerStateService
    {
        private readonly IPlayerRepository<PlayerState> _playerStateRepository;

        public PlayerStateService(IPlayerRepository<PlayerState> playerStateRepository)
        {
            _playerStateRepository = playerStateRepository;
        }

        public int GetMoney(PlayerType player)
        {
            return _playerStateRepository.GetByPlayer(player)?.Money?.Value ?? 0;
        }

        public void AddMoney(PlayerType player, int amount)
        {
            _playerStateRepository.GetByPlayer(player).Money.Value += amount;
        }

        public void SpendMoney(PlayerType player, int amount)
        {
            if (_playerStateRepository.GetByPlayer(player).Money.Value < amount)
                throw new Exception("Not enough money");

            _playerStateRepository.GetByPlayer(player).Money.Value -= amount;
        }

        public bool CanSpendMoney(PlayerType player, int amount)
        {
            return _playerStateRepository.GetByPlayer(player).Money.Value >= amount;
        }
    }
}
