﻿using System;
using Unity.Netcode;

namespace Assets.Game.Models.Utils
{
    public struct Log : INetworkSerializable, IEquatable<Log>
    {
        public int Player;

        public int CardType;

        public Log(int player, int cardType)
        {
            Player = player;
            CardType = cardType;
        }

        public bool Equals(Log other)
        {
            return CardType == other.CardType && Player == other.Player;
        }

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref Player);
            serializer.SerializeValue(ref CardType);
        }
    }
}