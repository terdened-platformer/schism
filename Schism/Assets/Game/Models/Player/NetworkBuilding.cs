﻿using System;
using Assets.Common.Constants;
using Assets.Game.Constants;
using Unity.Netcode;

namespace Assets.Game.Models.Player
{
    public struct NetworkBuilding : INetworkSerializable, IEquatable<NetworkBuilding>
    {
        public PlayerType Player => (PlayerType)_player;
        public BuildingType BuildingType => (BuildingType)_buildingType;

        private int _player;
        private int _buildingType;

        public NetworkBuilding(int player, int buildingType)
        {
            _player = player;
            _buildingType = buildingType;
        }

        public bool Equals(NetworkBuilding other)
        {
            return _buildingType == other._buildingType && _player == other._player;
        }

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref _player);
            serializer.SerializeValue(ref _buildingType);
        }
    }
}