﻿using Assets.Common.Constants;

namespace Assets.Game.Models.Player
{
    public interface IPlayer
    {
        PlayerType GetPlayer();
    }
}