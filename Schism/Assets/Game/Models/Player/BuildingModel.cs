﻿using Assets.Game.Constants;
using UnityEngine;

namespace Assets.Game.Models.Player
{
    [CreateAssetMenu(fileName = "New Building", menuName = "Building")]
    public class BuildingModel : ScriptableObject
    {
        public BuildingType Type;

        public Sprite Image;

        public string Title;

        public string Description;

        public int Cost;
    }
}
