﻿using Assets.Common.Constants;
using Assets.Game.Constants;
using Assets.Game.Controllers.Players;
using Unity.Netcode;

namespace Assets.Game.Models.Player
{
    public class PlayerBuildings: NetworkBehaviour
    {
        public PlayerType Player => _networkPlayer.Player;
        public NetworkList<NetworkBuilding> Buildings;

        private NetworkPlayer _networkPlayer;

        public void Awake()
        {
            Buildings = new NetworkList<NetworkBuilding>();
            _networkPlayer = GetComponent<NetworkPlayer>();
        }
        
        public void CreateBuilding(NetworkBuilding networkBuilding)
        {
            Buildings.Add(networkBuilding);
        }

        public int GetRewardBonus()
        {
            var bonus = 0;

            foreach (var building in Buildings)
            {
                switch (building.BuildingType)
                {
                    case BuildingType.Citadel1:
                        bonus++;
                        break;
                    case BuildingType.Citadel2:
                        bonus+=2;
                        break;
                    case BuildingType.Citadel3:
                        bonus+=3;
                        break;
                    case BuildingType.Citadel4:
                        bonus+=4;
                        break;

                }
            }
            
            return bonus;
        }
    }
}
