﻿using Assets.Common.Constants;
using Unity.Netcode;

namespace Assets.Game.Models.Player
{
    public class PlayerState: NetworkBehaviour, IPlayer
    {
        public NetworkVariable<ulong> ClientId;

        public PlayerType Player => (PlayerType)PlayerType.Value;

        public NetworkVariable<int> Money;

        public NetworkVariable<int> PlayerType;

        public void Init(PlayerType player, ulong clientId)
        {
            PlayerType.Value = (int)player;
            ClientId.Value = clientId;
            Money.Value = 0;
        }

        public PlayerType GetPlayer()
        {
            return (PlayerType)PlayerType.Value;
        }
    }
}
