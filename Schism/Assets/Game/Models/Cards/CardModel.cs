using Assets.Common.Constants;
using Assets.Game.Constants;
using UnityEngine;

namespace Assets.Game.Models.Cards
{
    [CreateAssetMenu(fileName = "New Card", menuName = "Card")]
    public class CardModel : ScriptableObject
    {
        public CardType Type;

        public Sprite Image;

        public string Title;

        public string Description;

        public int Cost;
    }
}
