using Assets.Common.Constants;
using Assets.Game.Constants;
using UnityEngine;

namespace Assets.Game.Models.Cards
{
    [CreateAssetMenu(fileName = "New Ability Card", menuName = "Ability Card")]
    public class AbilityCardModel : ScriptableObject
    {
        public AbilityCardType Type;

        public Sprite Image;

        public string Title;

        public string Description;

        public float CoolDown;
    }
}
