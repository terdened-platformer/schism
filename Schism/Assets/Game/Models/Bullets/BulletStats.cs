﻿using Assets.Common.Constants;
using Assets.Game.Constants;
using Assets.Game.Models.Units;
using UnityEngine;

namespace Assets.Game.Models.Bullets
{
    public class BulletStats
    {
        public PlayerType Owner;
        public DamageType DamageType;
        public int Damage;
        public float Speed;

        public void Init(UnitStats unitStats, PlayerType owner)
        {
            Owner = owner;
            DamageType = unitStats.DamageType;

            Damage = unitStats.Damage + GetCriticalDamage(unitStats);
            Speed = 20.0f;
        }

        private int GetCriticalDamage(UnitStats unitStats)
        {
            return Random.Range(0, 1) < unitStats.CriticalValue ? (int)(unitStats.Damage * unitStats.CriticalValue) : 0;
        }
    }
}
