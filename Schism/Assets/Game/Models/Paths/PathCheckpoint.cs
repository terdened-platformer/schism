﻿using UnityEngine;

namespace Assets.Game.Models.Paths
{
    public class PathCheckpoint : MonoBehaviour
    {
        public int Index;

        public void OnDrawGizmos()
        {
            var gizmoColor = Gizmos.color;
            
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(transform.position, 1);

            Gizmos.color = gizmoColor;
        }
    }
}
