﻿using Assets.Game.Constants;
using UnityEngine;

namespace Assets.Game.Models.Units
{
    public class UnitStats : MonoBehaviour
    {
        public int Health;
        public int PhysicalArmor;
        public int MagicArmor;
        public float Regeneration;
        public float Evasion;

        public int Damage;
        public DamageType DamageType;
        public float AttackRate;
        public float AttackDistance;
        public bool IsAoe;
        public float AoeDistance;
        public float CriticalChance;
        public float CriticalValue;
        public bool CanAttackFly;
        public float Taunt;
        public bool IsProjectile;

        public float VisionDistance;

        public float MoveSpeed;
        public int UnitsCount;
        public bool IsFly;
    }
}
