﻿namespace Assets.Game.Constants
{
    public enum UnitType
    {
        Default = 0,
        Brute = 1,
        Aoe = 2,
        Swarm = 3,
        Grunt = 4,
        Hero = 5,
        Archer = 6
    }
}
