﻿namespace Assets.Game.Constants
{
    public enum DamageType
    {
        Physical = 0,
        Magical = 1
    }
}
