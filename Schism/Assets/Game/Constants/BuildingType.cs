﻿namespace Assets.Game.Constants
{
    public enum BuildingType
    {
        Citadel1 = 0,
        Citadel2 = 1,
        Citadel3 = 2,
        Citadel4 = 3,

        Crypt1 = 4,
        Crypt2 = 5
    }
}
