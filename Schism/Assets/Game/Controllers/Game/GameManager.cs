﻿using System;
using System.Linq;
using Assets.Common.Constants;
using Assets.Common.Models;
using Assets.Game.Models.Player;
using Assets.Game.Repositories.Players;
using Unity.Netcode;
using UnityEngine.SceneManagement;
using Zenject;
using NetworkPlayer = Assets.Game.Controllers.Players.NetworkPlayer;

namespace Assets.Game.Controllers.Game
{
    public class GameManager : NetworkBehaviour
    {
        public int PlayersCount = 4;

        public event Action GameStarted;
        
        private bool _isStarted;

        private IPlayerRepository<PlayerState> _playerStateRepository;

        [Inject]
        public void Construct(IPlayerRepository<PlayerState> playerStateRepository)
        {
            _playerStateRepository = playerStateRepository;
        }

        private void Start()
        {
            InitGame();
        }

        private void Update()
        {
            if (!NetworkManager.IsServer) return;

            HandleEndGame();
        }

        private void InitGame()
        {
            if (Global.IsServer)
            {
                NetworkManager.StartServer();
                NetworkManager.OnClientDisconnectCallback += OnClientDisconnected;
                NetworkManager.OnClientConnectedCallback += OnClientConnected;

                if (Global.IsDebug)
                {
                    StartGame();
                }
            }
            else
            {
                NetworkManager.StartClient();
            }
        }

        private void StartGame()
        {
            GameStarted?.Invoke();
        }

        private void HandleEndGame()
        {
            var players = FindObjectsOfType<NetworkPlayer>();
            
            if (players.Count(_ => !_.IsDefeat()) == 1)
            {
                EndGame();
            }
        }

        private void EndGame()
        {
            EndGameClientRpc();
        }

        [ClientRpc]
        private void EndGameClientRpc()
        {
            NetworkManager.Shutdown();
            SceneManager.LoadScene("MainMenuScene");
        }

        private void OnClientDisconnected(ulong clientId)
        {
            if (Global.IsDebug || NetworkManager.ConnectedClients.Any()) return;
            
            NetworkManager.Shutdown();
            SceneManager.LoadScene("MainMenuScene");
        }

        private void OnClientConnected(ulong clientId)
        {
            var playerController = FindObjectsOfType<NetworkPlayer>()
                .FirstOrDefault(_ => _.GetComponent<NetworkBehaviour>().OwnerClientId == clientId);

            if (playerController == null) return;

            if (_playerStateRepository.Get().Count(_ => _.Player == playerController.Player) > 1)
            {
                playerController.PlayerType.Value = (int)PlayerType.Player2;
            }
            
            var playerState = playerController.GetComponent<PlayerState>();
            playerState.Init(playerController.Player, clientId);

            if (!_isStarted && _playerStateRepository.Get().Count() == PlayersCount)
            {
                _isStarted = true;
                StartGame();
            }
        }
    }
}
