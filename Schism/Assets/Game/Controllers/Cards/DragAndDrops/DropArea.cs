﻿using Assets.Common.Constants;
using Assets.Game.Constants;
using Assets.Game.Controllers.Buildings;
using UnityEngine;

namespace Assets.Game.Controllers.Cards
{
    [RequireComponent(typeof(BoxCollider))]
    [RequireComponent(typeof(MeshRenderer))]
    public class DropArea: MonoBehaviour
    {
        public DropAreaType Type;
        public PlayerType Owner;
        public Barrack Barrack;

        public void Start()
        {
            gameObject.layer = LayerMask.NameToLayer(Layers.DropArea);
        }

        public void Show()
        {
            GetComponent<BoxCollider>().enabled = true;
            GetComponent<MeshRenderer>().enabled = true;
        }

        public void Hide()
        {
            GetComponent<BoxCollider>().enabled = false;
            GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
