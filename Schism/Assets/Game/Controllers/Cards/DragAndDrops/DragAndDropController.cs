﻿using System;
using System.Linq;
using Assets.Game.Constants;
using Unity.Netcode;
using UnityEngine;
using Zenject;
using NetworkPlayer = Assets.Game.Controllers.Players.NetworkPlayer;

namespace Assets.Game.Controllers.Cards
{
    public class DragAndDropController: MonoBehaviour
    {
        public Action MouseDown;
        public Action MouseUp;
        public Action<DropArea> Drop;

        private Vector3 _screenPoint;
        private Vector3 _offset;
        private DropArea _activeDropArea;

        private void OnMouseDown()
        {
            _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));

            MouseDown?.Invoke();
            ShowDropPlaces();
        }

        private NetworkPlayer GetNetworkPlayer() =>
            FindObjectsOfType<NetworkPlayer>()
                .FirstOrDefault(_ => _.GetComponent<NetworkObject>().IsLocalPlayer);

        private void ShowDropPlaces()
        {
            var dropPlaces = FindObjectsOfType<DropArea>().Where(_ => _.Type == DropAreaType.Barrack);

            foreach (var dropPlace in dropPlaces)
            {
                if (GetNetworkPlayer().Player != dropPlace.Owner) continue;

                dropPlace.Show();
            }
        }

        private void HideDropPlaces()
        {
            var dropPlaces = FindObjectsOfType<DropArea>();

            foreach (var dropPlace in dropPlaces)
            {
                dropPlace.Hide();
            }
        }

        private void OnMouseUp()
        {
            HideDropPlaces();

            if (_activeDropArea != null)
            {
                Drop?.Invoke(_activeDropArea);
            }
            else
            {
                MouseUp?.Invoke();
            }
        }

        private void OnMouseDrag()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            var dropAreaMask = LayerMask.GetMask(Layers.DropArea);

            if (Physics.Raycast(ray, out var hit, 100f, dropAreaMask))
            {
                _activeDropArea = hit.transform.gameObject.GetComponent<DropArea>();

                transform.position = hit.point + Vector3.up;
            }
            else
            {
                var curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
                var curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset;
                transform.position = curPosition;

                _activeDropArea = null;
            }
        }
    }
}
