﻿using UnityEngine;

namespace Assets.Game.Controllers.Cards
{
    public class CardPlaceholder : MonoBehaviour
    {
        private CardController _cardController;
        
        public void SetCard(CardController cardController)
        {
            if (!IsEmpty())
            {
                Clear();
            }

            _cardController = cardController;
            _cardController.CardResetPosition += MoveCardInPlaceholder;

            MoveCardInPlaceholder();
        }

        public CardController GetCard()
        {
            return _cardController;
        }

        public void Clear()
        {
            if (!IsEmpty())
            {
                _cardController.CardResetPosition -= MoveCardInPlaceholder;
            }

            _cardController = null;
        }

        public bool IsEmpty()
        {
            return _cardController == null;
        }

        private void MoveCardInPlaceholder()
        {
            _cardController.transform.position = transform.position;
        }
    }
}
