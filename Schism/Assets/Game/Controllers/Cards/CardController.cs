﻿using System;
using System.Linq;
using Assets.Common.Constants;
using Assets.Game.Controllers.Buildings;
using Assets.Game.Controllers.Common;
using Assets.Game.Models.Cards;
using Assets.Game.Models.Player;
using Unity.Netcode;
using UnityEngine;
using Zenject;
using NetworkPlayer = Assets.Game.Controllers.Players.NetworkPlayer;

namespace Assets.Game.Controllers.Cards
{
    [RequireComponent(typeof(DragAndDropController))]
    public class CardController : MonoBehaviour
    {
        public CardType Type;
        public int Cost;

        public event Action<CardController, Barrack> CardDrop;
        public event Action CardResetPosition;
        
        private DragAndDropController _dragAndDropController;
        
        public void Init(CardModel card)
        {
            transform.Find("Title").GetComponent<TextMesh>().text = card.Title;
            transform.Find("Cost").GetComponent<TextMesh>().text = card.Cost.ToString();
            transform.Find("Description").GetComponent<TextMesh>().text = card.Description;
            transform.Find("Image").GetComponent<SpriteRenderer>().sprite = card.Image;
            
            _dragAndDropController = GetComponent<DragAndDropController>();
            _dragAndDropController.Drop += DropCard;
            _dragAndDropController.MouseUp += ResetPosition;

            Type = card.Type;
            Cost = card.Cost;
        }

        public void ResetPosition()
        {
            CardResetPosition?.Invoke();
        }

        private NetworkPlayer GetPlayerController() =>
            FindObjectsOfType<NetworkPlayer>()
                .FirstOrDefault(_ => _.GetComponent<NetworkObject>().IsLocalPlayer);

        private void DropCard(DropArea dropArea)
        {
            var playerState = GetPlayerController().GetComponent<PlayerState>();

            if (dropArea != null && playerState.Money.Value >= Cost)
            {
                if (Type != CardType.ReplaceAoeUnit && Type != CardType.ReplaceBruteUnit && Type != CardType.ReplaceSwarmUnit && Type != CardType.ReplaceGruntUnit && Type != CardType.ReplaceArcherUnit)
                {
                    GetPlayerController().SpendMoneyServerRpc(Cost);
                    CardDrop?.Invoke(this, dropArea.Barrack);

                    Destroy(gameObject);
                }
                else
                {
                    CardDrop?.Invoke(this, dropArea.Barrack);
                }
            }
            else
            {
                ResetPosition();
            }
        }
    }
}
