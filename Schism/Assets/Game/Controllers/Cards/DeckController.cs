﻿using System.Collections.Generic;
using System.Linq;
using Assets.Common.Models;
using Assets.Common.Utils;
using Assets.Game.Factories.Cards;
using Assets.Game.Models.Cards;
using UnityEngine;
using Zenject;

namespace Assets.Game.Controllers.Cards
{
    public class DeckController: MonoBehaviour
    {
        private const int DeckSize = 30;
        private List<CardModel> _deck;
        private ICardSoFactory _cardSoFactory;

        [Inject]
        private void Construct(ICardSoFactory cardSoFactory)
        {
            _cardSoFactory = cardSoFactory;
        }

        public CardModel TopDeck()
        {
            var topDeckCard = _deck.First();
            _deck.RemoveAt(0);

            return topDeckCard;
        }

        private void Start()
        {
            InitCustomDeck();
        }

        private void InitCustomDeck()
        {
            if (Global.IsServer)
                return;

            _deck = new List<CardModel>();

            for (var i = 0; i < DeckSize; i++)
            {
                var randomIndex = Random.Range(0, Global.SelectedDeck.Cards.Count);

                var card = _cardSoFactory.Create(Global.SelectedDeck.Cards[randomIndex]);
                
                if (card != null)
                {
                    _deck.Add(card);
                }
            }

            if (_deck.Count == 0)
                return;

            _deck.Shuffle();
        }
    }
}
