﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Game.Controllers.Buildings;
using Assets.Game.Controllers.Common;
using Assets.Game.Controllers.Game;
using Assets.Game.Factories.Cards;
using Assets.Game.Models.Cards;
using Assets.Game.View.ReplaceUnitPanel;
using Unity.Netcode;
using UnityEngine;
using Zenject;

namespace Assets.Game.Controllers.Cards
{
    public class HandController: MonoBehaviour
    {
        public CardController CardBuffer;
        public List<CardPlaceholder> CardPlaceholders;

        private const float SpawnRate = 30f;

        public event Action<CardController, Barrack> CardDrop;

        private const int DraftSize = 3;
        private const int HandSize = 5;
        private float _currentSpawnRate = 30f;
        private List<CardController> _cards;

        private DeckController _deckController;
        private ICardFactory _cardFactory;

        [Inject]
        private void Construct(DeckController deckController,
            ICardFactory cardFactory,
            ReplaceUnitPanelView replaceUnitPanelView,
            GameManager gameManager)
        {
            _deckController = deckController;
            _cardFactory = cardFactory;

            _cards = new List<CardController>();
        }

        private void Start()
        {
            if (NetworkManager.Singleton.IsServer)
                return;

            StartCoroutine(Draft());
        }
        
        public void TopDeck()
        {
            if (_cards.Count >= HandSize)
                return;

            var card = _deckController.TopDeck();
            var cardInstance = InstantiateCard(card);

            _cards.Add(cardInstance.GetComponent<CardController>());
            OrderCards();
        }

        public void RemoveCard(CardController card)
        {
            var index = _cards.IndexOf(card);
            CardPlaceholders[index].Clear();

            _cards.Remove(card);
            Destroy(card.gameObject);
            OrderCards();
        }

        private void Update()
        {
            HandleTopDeck();
        }

        private void HandleTopDeck()
        {
            if (NetworkManager.Singleton.IsServer)
                return;

            _currentSpawnRate -= Time.deltaTime;

            if (!(_currentSpawnRate <= 0)) return;

            _currentSpawnRate = SpawnRate;
            TopDeck();
        }

        private void OrderCards()
        {
            for (var i = 0; i < CardPlaceholders.Count; i++)
            {
                if (CardPlaceholders[i].IsEmpty() && i < CardPlaceholders.Count - 1 && !CardPlaceholders[i + 1].IsEmpty())
                {
                    CardPlaceholders[i].SetCard(CardPlaceholders[i + 1].GetCard());
                    CardPlaceholders[i + 1].Clear();
                }
            }
        }
        
        private IEnumerator Draft()
        {
            yield return null;

            for (var i = 0; i < DraftSize; i++)
            {
                TopDeck();
            }
        }

        private GameObject InstantiateCard(CardModel card)
        {
            var cardController = _cardFactory.Create(card);
            CardPlaceholders[_cards.Count].SetCard(cardController);
            cardController.CardDrop += (c, b) => CardDrop?.Invoke(c, b);

            return cardController.gameObject;
        }
    }
}
