using System.Collections.Generic;
using Assets.Common.Constants;
using Assets.Common.Utils.AI;
using Assets.Game.Constants;
using Assets.Game.Controllers.Common;
using Assets.Game.Controllers.Game;
using Assets.Game.Factories.Units;
using Unity.Netcode;
using UnityEngine;
using Zenject;

namespace Assets.Game.Controllers.Buildings
{
    public class Barrack : NetworkBehaviour
    {
        public float SpawnRate = 15;
        
        public List<UnitType> UnitWave;

        public NetworkList<int> UnitWaveNet;

        public ColliderTrigger LineCollider;
        public PlayerType Owner;
        public Color PlayerColor = Color.gray;
        public GameObject[] SpawnPoints;
        public bool GameStarted;

        [SerializeField]
        private float _currentRate;
        private int _currentUnitToReplace;
        private IUnitFactory _unitFactory;
        private GameManager _gameManager;

        [Inject]
        private void Construct(IUnitFactory unitFactory,
            GameManager gameManager)
        {
            _gameManager = gameManager;
            _unitFactory = unitFactory;

            _gameManager.GameStarted += StartGame;
        }

        public void Awake()
        {
            _currentRate = SpawnRate;

            UnitWaveNet = new NetworkList<int>();
            UnitWaveNet.OnListChanged += ListChanged;

            UnitWave = new List<UnitType>
            {
                UnitType.Default,
                UnitType.Default,
                UnitType.Default
            };
        }

        private void Start()
        {
            LineCollider = GetComponentInChildren<ColliderTrigger>();
        }

        private void ListChanged(NetworkListEvent<int> listEvent)
        {
            if (!IsServer)
            {
                Debug.Log(UnitWaveNet.Count);
            }
        }

        
        public void StartGame()
        {
            foreach (var unit in UnitWave)
            {
                UnitWaveNet.Add((int)unit);
            }

            GameStarted = true;
        }

        private void Update()
        {
            if (!NetworkManager.IsServer || !GameStarted)
                return;

            _currentRate += Time.deltaTime;

            if (!(_currentRate >= SpawnRate)) return;

            _currentRate = 0;

            for (var i = 0; i < UnitWave.Count; i++)
            {
                if (UnitWave[i] != UnitType.Swarm)
                {
                    SpawnUnit(i, UnitWave[i]);
                }
                else
                {
                    SpawnUnit(i, UnitWave[i]);
                    SpawnUnit(i, UnitWave[i]);
                    SpawnUnit(i, UnitWave[i]);
                }
            }
        }

        [ClientRpc]
        private void SpawnUnitClientRpc(ulong itemNetId)
        {
            var netObj = NetworkManager.SpawnManager.SpawnedObjects[itemNetId];

            var unit = netObj.gameObject;

            var meshRenderers = unit.GetComponentsInChildren<SkinnedMeshRenderer>();

            foreach (var meshRenderer in meshRenderers)
            {
                meshRenderer.material.SetColor("PlayerColor", PlayerColor);
            }
        }

        public void SpawnUnit(int unitIndex, UnitType type)
        {
            var unit = _unitFactory.Create(type, SpawnPoints[unitIndex].transform, Owner, PlayerColor);
            var itemNetId = unit.GetComponent<NetworkObject>().NetworkObjectId;
            SpawnUnitClientRpc(itemNetId);
        }

        public void ReplaceUnit(UnitType type, int index)
        {
            UnitWave[index] = type;
            UnitWaveNet[index] = (int)type;

            if (_currentUnitToReplace >= UnitWave.Count)
            {
                _currentUnitToReplace = 0;
            }
        }

        public void UpgradeBarrack()
        {
            UnitWave.Add(UnitType.Default);
            UnitWaveNet.Add((int)UnitType.Default);
            GetComponent<HealthPower>().MaxHealth.Value += 150;
        }
    }
}
