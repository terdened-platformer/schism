using System.Linq;
using Assets.Common.Constants;
using Assets.Game.Constants;
using Assets.Game.Models.Player;
using Assets.Game.Models.Units;
using Unity.Netcode;
using UnityEngine;
using NetworkPlayer = Assets.Game.Controllers.Players.NetworkPlayer;

namespace Assets.Game.Controllers.Common
{
    public class HealthPower : NetworkBehaviour
    {
        public int Reward = 5;
        public int PhysicalArmor;
        public int MagicArmor;
        public float Regeneration;
        public float Evasion;
        public bool AlwaysDisplayHpBar;
        public bool IsFly;
        public float Taunt;

        public NetworkVariable<int> Health;
        public NetworkVariable<int> MaxHealth;

        private float _regenerateCurrentTick;

        private RectTransform _hpBar;
        private GameObject _hpBarCanvas;

        public override void OnNetworkSpawn()
        {
            if (!IsServer)
                return;

            var stats = GetComponent<UnitStats>();

            if (stats == null)
                return;

            Health.Value = stats.Health;
            MaxHealth.Value = stats.Health;
            PhysicalArmor = stats.PhysicalArmor;
            MagicArmor = stats.MagicArmor;
            Regeneration = stats.Regeneration;
            Evasion = stats.Evasion;
            IsFly = stats.IsFly;
            Taunt = stats.Taunt;
        }

        public void Start()
        {
            var hpBarCanvas = Resources.Load<GameObject>("Prefabs/HpBarCanvas");

            _hpBarCanvas = Instantiate(hpBarCanvas, transform);
            _hpBarCanvas.transform.Translate(Vector3.up * 3);
            _hpBar = _hpBarCanvas.transform.Find("HpBar").gameObject.GetComponent<RectTransform>();
            _hpBarCanvas.SetActive(AlwaysDisplayHpBar);
        }

        public void Update()
        {
            if (!IsServer)
                return;

            HandleRegenerate();
        }

        public void LateUpdate()
        {
            HandleHpBar();
        }

        public void DealDamage(int damageAmount, PlayerType attacker, DamageType damageType)
        {
            if (IsEvaded())
                return;

            damageAmount = (int)(damageAmount * GetArmorK(damageType));

            Health.Value -= damageAmount;

            if (Health.Value > 0) return;

            RewardAttacker(attacker);
            Destroy(gameObject);
        }

        public void RewardAttacker(PlayerType attacker)
        {
            var attackerPlayer = FindObjectsOfType<NetworkPlayer>().FirstOrDefault(x => x.Player == attacker);

            if (attackerPlayer == null)
                return;

            var bonusReward = attackerPlayer.GetComponent<PlayerBuildings>()?.GetRewardBonus() ?? 0;

            attackerPlayer.AddMoney(Reward + bonusReward);
        }

        private float GetArmorK(DamageType damageType)
        {
            var armor = damageType == DamageType.Physical ? PhysicalArmor : MagicArmor;
            return armor >= 0 ? (100f / (100f + armor > 99 ? 99f : armor)) : (2f - 100f / (100f - armor < -99 ? -99f : armor));
        }

        private bool IsEvaded()
        {
            if (Evasion <= 0)
                return false;

            var value = Random.Range(0, 1);

            return Evasion > value;
        }

        private void HandleRegenerate()
        {
            _regenerateCurrentTick += Time.deltaTime;

            if (!(_regenerateCurrentTick >= 1)) return;

            _regenerateCurrentTick = 0;

            Health.Value += (int)(MaxHealth.Value * Regeneration);

            if (Health.Value > MaxHealth.Value)
                Health.Value = MaxHealth.Value;
        }

        private void HandleHpBar()
        {
            if (!AlwaysDisplayHpBar)
            {
                if (Input.GetKeyDown(KeyCode.LeftAlt))
                {
                    _hpBarCanvas.SetActive(true);
                }

                if (Input.GetKeyUp(KeyCode.LeftAlt))
                {
                    _hpBarCanvas.SetActive(false);
                }
            }

            if (!AlwaysDisplayHpBar && !_hpBarCanvas.activeInHierarchy)
            {
                return;
            }

            _hpBarCanvas.SetActive(true);
            var hpBarWidth = 14 * ((float)Health.Value / (float)MaxHealth.Value);
            _hpBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, hpBarWidth);
        }
    }
}

