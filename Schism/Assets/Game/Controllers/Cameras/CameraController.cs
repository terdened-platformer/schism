﻿using UnityEngine;

namespace Assets.Game.Controllers.Cameras
{
    public class CameraController : MonoBehaviour
    {
        public float CameraZoomSpeed = 20;
        public float DragSpeed = 25;

        private Vector3 _dragOrigin;
        private Vector3 _cameraOrigin;

        public void Update()
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                transform.Translate(Vector3.down * CameraZoomSpeed * Time.deltaTime);
            }

            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                transform.Translate(Vector3.down * -CameraZoomSpeed * Time.deltaTime);
            }

            HandleMouseDrag();
        }


        private void HandleMouseDrag()
        {
            if (Input.GetMouseButtonDown(1))
            {
                _dragOrigin = Input.mousePosition;
                _cameraOrigin = transform.position;

                return;
            }

            if (!Input.GetMouseButton(1)) return;

            var diff = Camera.main.ScreenToViewportPoint(Input.mousePosition - _dragOrigin);

            transform.position = _cameraOrigin - new Vector3(diff.x, 0, diff.y) * DragSpeed;
        }
    }
}
