﻿using UnityEngine;

namespace Assets.Game.Controllers.Utils
{
    public class Billboard: MonoBehaviour
    {
        public void LateUpdate()
        {
            transform.LookAt(transform.position + Camera.main.transform.forward);
        }
    }
}
