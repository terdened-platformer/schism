﻿using UnityEngine;

namespace Assets.Game.Controllers.Utils
{
    [RequireComponent(typeof(MeshRenderer))]
    public class SelectableObject: MonoBehaviour
    {
        public Color SelectedColor = Color.magenta;
        private Color _initialColor;

        public void OnEnable()
        {
            _initialColor = GetComponent<MeshRenderer>().material.color;
        }

        public void Select()
        {
            GetComponent<MeshRenderer>().material.color = SelectedColor;

            foreach (var componentsInChild in GetComponentsInChildren<MeshRenderer>())
            {
                componentsInChild.material.color = SelectedColor;
            }
        }

        public void Unselect()
        {
            GetComponent<MeshRenderer>().material.color = _initialColor;

            foreach (var componentsInChild in GetComponentsInChildren<MeshRenderer>())
            {
                componentsInChild.material.color = _initialColor;
            }
        }
    }
}
