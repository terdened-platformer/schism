﻿using System;
using Assets.Game.Models.Utils;
using Unity.Netcode;

namespace Assets.Game.Controllers.Utils
{
    public class Logger: NetworkBehaviour
    {
        public NetworkList<Log> Logs;

        public event Action<Log> CardLog;

        public void Awake()
        {
            Logs = new NetworkList<Log>();
            Logs.OnListChanged += ListChanged;
        }

        [ServerRpc(RequireOwnership = false)]
        public void WriteLogServerRpc(Log log)
        {
            Logs.Add(log);
        }

        private void ListChanged(NetworkListEvent<Log> listEvent)
        {
            if (!IsServer)
            {
                CardLog?.Invoke(listEvent.Value);
            }
        }
    }
}
