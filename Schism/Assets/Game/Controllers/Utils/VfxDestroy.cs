﻿using Unity.Netcode;
using UnityEngine;
using UnityEngine.VFX;

namespace Assets.Game.Controllers.Utils
{
    [RequireComponent(typeof(VisualEffect))]
    public class VfxDestroy : NetworkBehaviour
    {
        private VisualEffect _visualEffect;

        public void Start()
        {
            _visualEffect = GetComponent<VisualEffect>();
            _visualEffect.Play();

            Destroy(this.gameObject, 2);
        }
    }
}
