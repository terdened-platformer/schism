﻿using System.Collections.Generic;
using System.Linq;
using Assets.Common.Constants;
using Assets.Common.Utils;
using Assets.Game.Controllers.Buildings;
using Assets.Game.Controllers.Bullets;
using Assets.Game.Controllers.Common;
using Assets.Game.Models.Bullets;
using Assets.Game.Models.Units;
using Assets.Game.Services.Units.AI;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Game.Controllers.Units
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(UnitStats))]
    public class SimpleUnitController : NetworkBehaviour
    {
        public PlayerType Owner;
        public Sprite Avatar;
        public GameObject AttackEffectPrefab;
        public GameObject ProjectilePrefab;

        private Animator _animator;
        private NetworkAnimator _networkAnimator;
        private List<HealthPower> _currentTargets;
        private HealthPower _attackPosition;
        private UnitStats _stat;
        private NavMeshAgent _navMeshAgent;


        public void Start()
        {
            var attackCollider = transform.Find("MeleeCollider").GetComponent<SphereCollider>();
            attackCollider.radius = _stat.AttackDistance;
            var visionCollider = transform.Find("VisionCollider").GetComponent<SphereCollider>();
            visionCollider.radius = _stat.VisionDistance;
            var animationEventEmitter = transform.Find("Geometry").GetComponent<AnimationEventEmitter>();
            animationEventEmitter.Rise += AttackCurrentTarget;
        }

        public override void OnNetworkSpawn()
        {
            Init();
        }

        public void Init()
        {
            _stat = GetComponent<UnitStats>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _animator = GetComponentInChildren<Animator>();
            _networkAnimator = GetComponentInChildren<NetworkAnimator>();

            if (!NetworkManager.Singleton.IsServer) return;

            _navMeshAgent.enabled = true;
            _navMeshAgent.speed = _stat.MoveSpeed;
        }

        public void StopMovement()
        {
            if (!NetworkManager.Singleton.IsServer)
                return;

            _navMeshAgent.avoidancePriority = 50;

            _navMeshAgent.isStopped = true;
            _animator.SetFloat("MoveSpeed", 0);
        }

        public void MoveTo(Vector3 position)
        {
            if (!NetworkManager.Singleton.IsServer)
                return;

            _navMeshAgent.avoidancePriority = 70;

            _navMeshAgent.isStopped = false;
            _navMeshAgent.SetDestination(position);
            _animator.SetFloat("MoveSpeed", _navMeshAgent.speed);
        }

        public void LookAtEnemy(GameObject enemy)
        {
            var lTargetDir = enemy.transform.position - transform.position;
            lTargetDir.y = 0.0f;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lTargetDir), Time.time * 0.01f);
        }

        public void Attack(HealthPower target, bool isAoe, float aoeRange)
        {
            if (!NetworkManager.Singleton.IsServer)
                return;

            if (isAoe)
            {
                _currentTargets = new List<HealthPower>();

                var hits = Physics
                    .OverlapSphere(target.gameObject.transform.position, aoeRange)
                    .Where(_ => _.GetComponent<HealthPower>() != null);

                foreach (var hit in hits)
                {
                    var simpleUnitAi = hit.GetComponent<SimpleUnitAI>();

                    if (simpleUnitAi != null && simpleUnitAi.Owner != Owner)
                    {
                        _currentTargets.Add(hit.GetComponent<HealthPower>());
                    }

                    var barrack = hit.GetComponent<Barrack>();

                    if (barrack != null && barrack.Owner != Owner)
                    {
                        _currentTargets.Add(hit.GetComponent<HealthPower>());
                    }
                }
            }
            else
            {
                _currentTargets = new List<HealthPower> {target};
            }

            _attackPosition = target;
            _animator.SetTrigger("AttackEnemy");
            _networkAnimator.SetTrigger("AttackEnemy");

            Invoke("SpawnAttackEffect", 0.5f);

        }

        private void AttackCurrentTarget()
        {
            if(!NetworkManager.IsServer)
                return;

            var selfEndurance = GetComponent<HealthPower>();

            if (selfEndurance == null || selfEndurance.Health.Value < 0)
                return;

            var center = transform.Find("Center");
            var bulletStats = new BulletStats();
            bulletStats.Init(_stat, Owner);

            foreach (var currentTarget in _currentTargets)
            {
                if (currentTarget == null || currentTarget.Health.Value <= 0) continue;

                if (!_stat.IsProjectile)
                {
                    currentTarget.DealDamage(_stat.Damage + GetCriticalDamage(), Owner, _stat.DamageType);
                } 
                else
                {
                    var projectileInstance = Instantiate(ProjectilePrefab, center?.position ?? gameObject.transform.position, Quaternion.identity);

                    projectileInstance.GetComponent<Projectile>().Init(bulletStats, currentTarget);
                    projectileInstance.GetComponent<NetworkObject>().Spawn();
                }
            }

        }

        private int GetCriticalDamage()
        {
            return Random.Range(0, 1) < _stat.CriticalValue ? (int)(_stat.Damage * _stat.CriticalValue) : 0;
        }

        private void SpawnAttackEffect()
        {
            if(AttackEffectPrefab == null || _attackPosition == null || _attackPosition.gameObject == null || !_attackPosition.gameObject.activeInHierarchy)
                return;

            var effectInstance = Instantiate(AttackEffectPrefab, _attackPosition.gameObject.transform.position, Quaternion.identity);
            effectInstance.GetComponent<NetworkObject>().Spawn();
        }
    }
}
