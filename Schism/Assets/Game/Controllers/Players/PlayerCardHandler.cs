﻿using System;
using Assets.Common.Constants;
using Assets.Game.Constants;
using Assets.Game.Controllers.Buildings;
using Assets.Game.Controllers.Cards;
using Assets.Game.Models.Utils;
using Assets.Game.View.ReplaceUnitPanel;
using UnityEngine;
using Zenject;
using Logger = Assets.Game.Controllers.Utils.Logger;

namespace Assets.Game.Controllers.Players
{
    public class PlayerCardHandler : MonoBehaviour
    {
        public NetworkPlayer NetworkPlayer;

        public event Action<Barrack, UnitType> SpawnUnit;
        public event Action<Barrack, UnitType, int> ReplaceUnit;
        public event Action<Barrack> UpgradeBarrack;
        public event Action<Barrack, CardType> AffectUnitsOnLine;
        
        private HandController _handController;
        private ReplaceUnitPanelView _replaceUnitPanelView;
        private Logger _logger;

        [Inject]
        private void Construct(ReplaceUnitPanelView replaceUnitPanelView,
            HandController handController,
            Logger logger)
        {
            _logger = logger;
            _handController = handController;
            _replaceUnitPanelView = replaceUnitPanelView;

            _replaceUnitPanelView.ReplaceUnit += (barrack, type, index) => {
                NetworkPlayer.SpendMoneyServerRpc(_handController.CardBuffer.Cost);
                logger.WriteLogServerRpc(new Log((int)NetworkPlayer.Player, (int)_handController.CardBuffer.Type));
                ReplaceUnit?.Invoke(barrack, type, index);

                _handController.RemoveCard(_handController.CardBuffer);
                _handController.CardBuffer = null;
            };

            _replaceUnitPanelView.Cancel += () => {
                _handController.CardBuffer.ResetPosition();
                _handController.CardBuffer = null;
            };

            _handController.CardDrop += OnCardDrop;
        }

        public void Init(NetworkPlayer networkPlayer)
        {
            NetworkPlayer = networkPlayer;
        }

        private void ShowReplaceUnitPanel(Barrack barrack, UnitType type)
        {
            _replaceUnitPanelView.Show(barrack, type);
        }

        private void OnCardDrop(CardController card, Barrack barrack)
        {
            switch (card.Type)
            {
                case CardType.DefaultUnit:
                    SpawnUnit?.Invoke(barrack, UnitType.Default);
                    break;
                case CardType.DefaultUnits:
                    SpawnUnit?.Invoke(barrack, UnitType.Default);
                    SpawnUnit?.Invoke(barrack, UnitType.Default);
                    break;
                case CardType.BruteUnit:
                    SpawnUnit?.Invoke(barrack, UnitType.Brute);
                    break;
                case CardType.AoeUnit:
                    SpawnUnit?.Invoke(barrack, UnitType.Aoe);
                    break;
                case CardType.SwarmUnit:
                    SpawnUnit?.Invoke(barrack, UnitType.Swarm);
                    SpawnUnit?.Invoke(barrack, UnitType.Swarm);
                    SpawnUnit?.Invoke(barrack, UnitType.Swarm);
                    break;

                case CardType.ReplaceBruteUnit:
                    _handController.CardBuffer = card;
                    ShowReplaceUnitPanel(barrack, UnitType.Brute);
                    break;
                case CardType.ReplaceAoeUnit:
                    _handController.CardBuffer = card;
                    ShowReplaceUnitPanel(barrack, UnitType.Aoe);
                    break;
                case CardType.ReplaceSwarmUnit:
                    _handController.CardBuffer = card;
                    ShowReplaceUnitPanel(barrack, UnitType.Swarm);
                    break;

                case CardType.UpgradeBarrack:
                    UpgradeBarrack?.Invoke(barrack);
                    break;

                case CardType.Hellfire:
                    AffectUnitsOnLine?.Invoke(barrack, card.Type);
                    break;

                case CardType.GruntUnit:
                    SpawnUnit?.Invoke(barrack, UnitType.Grunt);
                    break;
                case CardType.ReplaceGruntUnit:
                    _handController.CardBuffer = card;
                    ShowReplaceUnitPanel(barrack, UnitType.Grunt);
                    break;
                case CardType.HeroUnit:
                    SpawnUnit?.Invoke(barrack, UnitType.Hero);
                    break;


                case CardType.ArcherUnit:
                    SpawnUnit?.Invoke(barrack, UnitType.Archer);
                    break;
                case CardType.ReplaceArcherUnit:
                    _handController.CardBuffer = card;
                    ShowReplaceUnitPanel(barrack, UnitType.Archer);
                    break;
                default:
                    break;
            }


            if (card.Type != CardType.ReplaceAoeUnit && card.Type != CardType.ReplaceBruteUnit && card.Type != CardType.ReplaceSwarmUnit)
            {
                _logger.WriteLogServerRpc(new Log((int)NetworkPlayer.Player, (int)card.Type));
                _handController.RemoveCard(card);
            }
        }
    }
}
