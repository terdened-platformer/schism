﻿using System.Linq;
using Assets.Common.Constants;
using Assets.Game.Constants;
using Assets.Game.Controllers.Common;
using Assets.Game.Models.Player;
using Assets.Game.View.BarrackPanel;
using Assets.Game.View.BuildingPanel;
using Assets.Game.View.PlayerStatePanel;
using Unity.Netcode;
using UnityEngine;
using Zenject;
using Barrack = Assets.Game.Controllers.Buildings.Barrack;

namespace Assets.Game.Controllers.Players
{
    public class NetworkPlayer : NetworkBehaviour
    {
        public GameObject AttackEffectPrefab;
        public NetworkVariable<int> PlayerType;

        public PlayerType Player => (PlayerType)PlayerType.Value;

        private PlayerCardHandler _playerCardsController;
        private BarrackPanelView _barrackPanelView;
        private PlayerStatePanelView _playerStatePanelView;

        private PlayerState _playerState;
        private PlayerBuildings _playerBuildings;
        private BuildingPanelView _buildingPanelView;

        [Inject]
        private void Construct(PlayerCardHandler playerCardsController,
            BarrackPanelView barrackPanelView,
            PlayerStatePanelView playerStatePanelView,
            BuildingPanelView buildingPanelView)
        {
            _buildingPanelView = buildingPanelView;
            _playerStatePanelView = playerStatePanelView;
            _barrackPanelView = barrackPanelView;
            _playerCardsController = playerCardsController;

            _playerState = GetComponent<PlayerState>();
            _playerBuildings = GetComponent<PlayerBuildings>();
        }

        private void Start()
        {
            if (!IsLocalPlayer)
                return;

            Init();
        }

        private void Update()
        {
            if (!IsLocalPlayer)
                return;

            HandleUserInput();
        }

        private void HandleUserInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var hitsInfo = Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition)).OrderBy(_ => _.distance).ToArray();

                foreach (var hitInfo in hitsInfo)
                {
                    if (hitInfo.collider.isTrigger)
                        continue;

                    if (hitInfo.transform.gameObject.tag == "Barrack")
                    {
                        var barrack = hitInfo.transform.gameObject.GetComponent<Barrack>();

                        _barrackPanelView.Show(barrack, barrack.Owner == Player);
                    }
                    else
                    {
                        _barrackPanelView.Hide();
                    }

                    break;
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _barrackPanelView.Hide();
            }
        }

        public void Init()
        {
            _playerStatePanelView.Init(this);

            _playerCardsController.Init(this);
            _playerCardsController.SpawnUnit += SpawnExtraUnit;
            _playerCardsController.ReplaceUnit += ReplaceUnit;
            _playerCardsController.UpgradeBarrack += UpgradeBarrack;
            _playerCardsController.AffectUnitsOnLine += AffectUnitsOnLine;

            _buildingPanelView.Build += CreateBuildingServerRpc;
        }

        public bool IsDefeat()
        {
            return FindObjectsOfType<Barrack>().Any(_ => _.Owner == Player);
        }

        public void AddMoney(int amount)
        {
            _playerState.Money.Value += amount;
        }

        [ServerRpc(RequireOwnership = false)]
        private void CreateBuildingServerRpc(BuildingType type)
        {
            GetComponent<PlayerBuildings>().CreateBuilding(new NetworkBuilding((int)Player, (int)type));
        }

        [ServerRpc]
        public void SpendMoneyServerRpc(int amount)
        {
            _playerState.Money.Value -= amount;

            if (_playerState.Money.Value < 0)
                _playerState.Money.Value = 0;
        }

        [ServerRpc(RequireOwnership = false)]
        private void SpawnUnitServerRpc(ulong barrackNetId, UnitType type)
        {
            var netObj = NetworkManager.SpawnManager.SpawnedObjects[barrackNetId];
            netObj.GetComponent<Barrack>().SpawnUnit(0, type);
        }

        [ServerRpc(RequireOwnership = false)]
        private void ReplaceUnitServerRpc(ulong barrackNetId, UnitType type, int index)
        {
            var netObj = NetworkManager.SpawnManager.SpawnedObjects[barrackNetId];
            netObj.GetComponent<Barrack>().ReplaceUnit(type, index);
        }

        [ServerRpc(RequireOwnership = false)]
        private void UpgradeBarrackServerRpc(ulong barrackNetId)
        {
            var netObj = NetworkManager.SpawnManager.SpawnedObjects[barrackNetId];
            netObj.GetComponent<Barrack>().UpgradeBarrack();
        }

        [ServerRpc(RequireOwnership = false)]
        private void AffectUnitsOnLineServerRpc(ulong barrackNetId, CardType cardType)
        {
            var netObj = NetworkManager.SpawnManager.SpawnedObjects[barrackNetId];
            var lineCollider = netObj.GetComponent<Barrack>().LineCollider;

            foreach (var trigger in lineCollider.TriggerList)
            {
                if (trigger == null || trigger.gameObject == null || !trigger.gameObject.activeInHierarchy)
                    continue;

                var enduranceScript = trigger.GetComponent<HealthPower>();

                if (enduranceScript == null)
                    continue;

                switch (cardType)
                {
                    case CardType.Hellfire:
                        enduranceScript.DealDamage(50, Player, DamageType.Magical);
                        SpawnAttackEffect(enduranceScript.transform.position);
                        break;
                }
            }
        }

        private void SpawnExtraUnit(Barrack barrack, UnitType type)
        {
            SpawnUnitServerRpc(barrack.GetComponent<NetworkObject>().NetworkObjectId, type);
        }

        private void ReplaceUnit(Barrack barrack, UnitType type, int index)
        {
            ReplaceUnitServerRpc(barrack.GetComponent<NetworkObject>().NetworkObjectId, type, index);
        }

        private void UpgradeBarrack(Barrack barrack)
        {
            UpgradeBarrackServerRpc(barrack.GetComponent<NetworkObject>().NetworkObjectId);
        }

        private void AffectUnitsOnLine(Barrack barrack, CardType cardType)
        {
            AffectUnitsOnLineServerRpc(barrack.GetComponent<NetworkObject>().NetworkObjectId, cardType);
        }
        
        private void SpawnAttackEffect(Vector3 position)
        {
            if (AttackEffectPrefab == null)
                return;

            var effectInstance = Instantiate(AttackEffectPrefab, position, Quaternion.identity);
            effectInstance.GetComponent<NetworkObject>().Spawn();
        }
    }
}
