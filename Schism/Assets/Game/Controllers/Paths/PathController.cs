﻿using Assets.Common.Constants;
using UnityEngine;

namespace Assets.Game.Controllers.Paths
{
    public class PathController : MonoBehaviour
    {
        public GameObject Path;
        public PlayerType Owner;
    }
}
