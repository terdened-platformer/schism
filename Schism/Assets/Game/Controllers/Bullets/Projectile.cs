﻿using Assets.Common.Utils.AI;
using Assets.Game.Controllers.Common;
using Assets.Game.Models.Bullets;
using Unity.Netcode;
using UnityEngine;

namespace Assets.Game.Controllers.Bullets
{
    public class Projectile : NetworkBehaviour
    {
        private BulletStats _stat;
        private HealthPower _target;
        private ColliderTrigger _colliderTrigger;

        public void Init(BulletStats stat, HealthPower target)
        {
            _stat = stat;
            _target = target;
            LookAtTarget();
        }

        public void Start()
        {
            _colliderTrigger = GetComponent<ColliderTrigger>();
        }

        public void Update()
        {
            if (!IsServer)
                return;


            if (_target == null || _target.gameObject == null || !_target.gameObject.activeInHierarchy)
            {
                Destroy(this.gameObject);
                return;
            }

            if (IsCollideTarget())
            {
                DealDamage();
                Destroy(this.gameObject);
            }
            else
            {
                HandleMovement();
            }
        }

        private bool IsCollideTarget()
        {
            var center = _target.transform.Find("Center");
            return Vector3.Distance(center?.position ?? _target.transform.position, transform.position) < 0.01f;
        }

        private void DealDamage()
        {
            _target.DealDamage(_stat.Damage, _stat.Owner, _stat.DamageType);
        }

        private void HandleMovement()
        {
            var center = _target.transform.Find("Center");
            transform.position = Vector3.MoveTowards(transform.position, center?.position ?? _target.transform.position, _stat.Speed * Time.deltaTime);
        }

        public void LookAtTarget()
        {
            var lTargetDir = _target.transform.position - transform.position;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lTargetDir), 360);
        }
    }
}
