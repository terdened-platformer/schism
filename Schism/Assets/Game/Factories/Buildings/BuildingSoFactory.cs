﻿using System.Collections.Generic;
using System.Linq;
using Assets.Game.Constants;
using Assets.Game.Models.Player;
using UnityEngine;

namespace Assets.Game.Factories.Buildings
{
    public interface IBuildingSoFactory
    {
        void Load();

        BuildingModel Create(BuildingType type);
    }

    public class BuildingSoFactory : IBuildingSoFactory
    {
        private const string Citadel01Path = "Prefabs/Buildings/Citadel01";
        private const string Citadel02Path = "Prefabs/Buildings/Citadel02";
        private const string Citadel03Path = "Prefabs/Buildings/Citadel03";
        private const string Citadel04Path = "Prefabs/Buildings/Citadel04";
        private const string Crypt01Path = "Prefabs/Buildings/Crypt01";
        private const string Crypt02Path = "Prefabs/Buildings/Crypt01";

        private List<BuildingModel> _buildingPrefabs;

        public void Load()
        {
            _buildingPrefabs = new List<BuildingModel>
            {
                Resources.Load<BuildingModel>(Citadel01Path),
                Resources.Load<BuildingModel>(Citadel02Path),
                Resources.Load<BuildingModel>(Citadel03Path),
                Resources.Load<BuildingModel>(Citadel04Path),
                Resources.Load<BuildingModel>(Crypt01Path),
                Resources.Load<BuildingModel>(Crypt02Path)
            };
        }

        public BuildingModel Create(BuildingType type)
        {
            return _buildingPrefabs.FirstOrDefault(_ => _.Type == type);
        }
    }
}
