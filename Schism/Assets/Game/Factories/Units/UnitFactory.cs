﻿using System.Collections.Generic;
using Assets.Common.Constants;
using Assets.Game.Constants;
using Assets.Game.Controllers.Units;
using Assets.Game.Services.Units.AI;
using Unity.Netcode;
using UnityEngine;
using Zenject;

namespace Assets.Game.Factories.Units
{
    public interface IUnitFactory
    {
        void Load();

        SimpleUnitController Create(UnitType type, Transform at, PlayerType owner, Color playerColor);

        Sprite GetAvatar(UnitType type);
    }

    public class UnitFactory: IUnitFactory
    {
        private readonly DiContainer _diContainer;

        private const string AoeUnitPrefabPath = "Prefabs/Units/AoeUnit01";
        private const string ArcherUnitPrefabPath = "Prefabs/Units/ArcherUnit01";
        private const string BruteUnitPrefabPath = "Prefabs/Units/BruteUnit01";
        private const string DefaultUnitPrefabPath = "Prefabs/Units/DefaultUnit01";
        private const string GruntUnitPrefabPath = "Prefabs/Units/GruntUnit01";
        private const string HeroUnitPrefabPath = "Prefabs/Units/HeroUnit01";
        private const string SwarmUnitPrefabPath = "Prefabs/Units/SwarmUnit01";
        
        private Dictionary<UnitType, GameObject> _unitPrefabs;

        public UnitFactory(DiContainer diContainer)
        {
            _diContainer = diContainer;
        }

        public void Load()
        {
            _unitPrefabs = new Dictionary<UnitType, GameObject>
            {
                [UnitType.Default] = Resources.Load<GameObject>(DefaultUnitPrefabPath),
                [UnitType.Brute] = Resources.Load<GameObject>(BruteUnitPrefabPath),
                [UnitType.Aoe] = Resources.Load<GameObject>(AoeUnitPrefabPath),
                [UnitType.Swarm] = Resources.Load<GameObject>(SwarmUnitPrefabPath),
                [UnitType.Grunt] = Resources.Load<GameObject>(GruntUnitPrefabPath),
                [UnitType.Hero] = Resources.Load<GameObject>(HeroUnitPrefabPath),
                [UnitType.Archer] = Resources.Load<GameObject>(ArcherUnitPrefabPath)
            };
        }

        public SimpleUnitController Create(UnitType type, Transform at, PlayerType owner, Color playerColor)
        {
            var prefab = _unitPrefabs[type];
            prefab.SetActive(false);
            var unitInstance = _diContainer.InstantiatePrefab(_unitPrefabs[type]);
            unitInstance.SetActive(true);
            unitInstance.gameObject.transform.position = at.position;
            unitInstance.gameObject.transform.rotation = at.rotation;

            var ai = unitInstance.gameObject.GetComponent<SimpleUnitAI>();
            ai.Owner = owner;

            var controller = unitInstance.gameObject.GetComponent<SimpleUnitController>();
            controller.Owner = owner;

            unitInstance.GetComponent<NetworkObject>().Spawn();
            
            unitInstance.layer = owner switch
            {
                PlayerType.Player1 => LayerMask.NameToLayer("Player1Unit"),
                PlayerType.Player2 => LayerMask.NameToLayer("Player2Unit"),
                PlayerType.Player3 => LayerMask.NameToLayer("Player3Unit"),
                PlayerType.Player4 => LayerMask.NameToLayer("Player4Unit"),
                _ => unitInstance.layer
            };
            
            var meshRenderers = unitInstance.GetComponentsInChildren<SkinnedMeshRenderer>();
            foreach (var meshRenderer in meshRenderers)
            {
                meshRenderer.material.SetColor("playerColor", playerColor);
            }

            ai.Init();
            controller.Init();

            return controller;
        }

        public Sprite GetAvatar(UnitType type)
        {
            return _unitPrefabs[type].GetComponent<SimpleUnitController>().Avatar;
        }
    }
}
