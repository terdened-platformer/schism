﻿using System.Collections.Generic;
using System.Linq;
using Assets.Common.Constants;
using Assets.Game.Models.Cards;
using UnityEngine;

namespace Assets.Game.Factories.Cards
{
    public interface ICardSoFactory
    {
        void Load();

        CardModel Create(CardType type);
    }

    public class CardSoFactory : ICardSoFactory
    {
        private const string DefaultUnit2Path = "Prefabs/Cards/2DefaultUnit";
        private const string AoeUnitPath = "Prefabs/Cards/AoeUnit";
        private const string ArcherUnitPath = "Prefabs/Cards/ArcherUnit";
        private const string BruteUnitPath = "Prefabs/Cards/BruteUnit";
        private const string DefaultUnitPath = "Prefabs/Cards/DefaultUnit";
        private const string GruntUnitPath = "Prefabs/Cards/GruntUnit";
        private const string HellfirePath = "Prefabs/Cards/Hellfire";
        private const string HeroUnitPath = "Prefabs/Cards/HeroUnit";
        private const string ReplaceAoeUnitPath = "Prefabs/Cards/ReplaceAoeUnit";
        private const string ReplaceArcherUnitPath = "Prefabs/Cards/ReplaceArcherUnit";
        private const string ReplaceBruteUnitPath = "Prefabs/Cards/ReplaceBruteUnit";
        private const string ReplaceGruntUnitPath = "Prefabs/Cards/ReplaceGruntUnit";
        private const string ReplaceSwarmUnitPath = "Prefabs/Cards/ReplaceSwarmUnit";
        private const string SwarmUnitPath = "Prefabs/Cards/SwarmUnit";
        private const string UpgradeBarrackPath = "Prefabs/Cards/UpgradeBarrack";

        private List<CardModel> _cardPrefabs;

        public void Load()
        {
            _cardPrefabs = new List<CardModel>
            {
                Resources.Load<CardModel>(DefaultUnit2Path),
                Resources.Load<CardModel>(AoeUnitPath),
                Resources.Load<CardModel>(ArcherUnitPath),
                Resources.Load<CardModel>(BruteUnitPath),
                Resources.Load<CardModel>(DefaultUnitPath),
                Resources.Load<CardModel>(GruntUnitPath),
                Resources.Load<CardModel>(HellfirePath),
                Resources.Load<CardModel>(HeroUnitPath),
                Resources.Load<CardModel>(ReplaceAoeUnitPath),
                Resources.Load<CardModel>(ReplaceArcherUnitPath),
                Resources.Load<CardModel>(ReplaceBruteUnitPath),
                Resources.Load<CardModel>(ReplaceGruntUnitPath),
                Resources.Load<CardModel>(ReplaceSwarmUnitPath),
                Resources.Load<CardModel>(SwarmUnitPath),
                Resources.Load<CardModel>(UpgradeBarrackPath)
            };
        }

        public CardModel Create(CardType type)
        {
            return _cardPrefabs.FirstOrDefault(_ => _.Type == type);
        }
    }
}
