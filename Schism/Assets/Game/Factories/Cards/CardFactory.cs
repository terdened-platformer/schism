﻿using Assets.Game.Controllers.Cards;
using Assets.Game.Controllers.Players;
using Assets.Game.Models.Cards;
using UnityEngine;
using Zenject;

namespace Assets.Game.Factories.Cards
{
    public interface ICardFactory
    {
        void Load();
        CardController Create(CardModel card);
    }

    public class CardFactory: ICardFactory
    {
        private const string CardPrefabPath = "Prefabs/Card";
        private readonly DiContainer _diContainer;
        private readonly PlayerCardHandler _playerCardsController;

        private GameObject _cardPrefab;
        
        public CardFactory(DiContainer diContainer,
            PlayerCardHandler playerCardsController)
        {
            _diContainer = diContainer;
            _playerCardsController = playerCardsController;
        }

        public void Load()
        {
            _cardPrefab = Resources.Load<GameObject>(CardPrefabPath);
        }

        public CardController Create(CardModel card)
        {
            var cardController = _diContainer.InstantiatePrefab(_cardPrefab, _playerCardsController.transform)
                .GetComponent<CardController>();
            cardController.Init(card);

            return cardController;
        }
    }
}
