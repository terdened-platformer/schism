﻿using System.Collections.Generic;
using Assets.Common.Constants;
using Assets.Game.Models.Utils;
using Assets.Game.Services.Players;
using Assets.Game.View.BuildingPanel;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;
using Logger = Assets.Game.Controllers.Utils.Logger;
using NetworkPlayer = Assets.Game.Controllers.Players.NetworkPlayer;

namespace Assets.Game.View.PlayerStatePanel
{
    public class PlayerStatePanelView : MonoBehaviour
    {
        public VisualTreeAsset LogAsset;

        private VisualElement _playerStatePanel;
        private Label _moneyLabel;
        private ListView _logList;
        private readonly List<Log> _logs = new();
        private Logger _logger;
        private Button _upgradeButton;
        private BuildingPanelView _buildingPanelView;
        private IPlayerStateService _playerStateService;
        private NetworkPlayer _networkPlayer;

        [Inject]
        private void Construct(Logger logger,
            BuildingPanelView buildingPanelView,
            IPlayerStateService playerStateService)
        {
            _playerStateService = playerStateService;
            _buildingPanelView = buildingPanelView;
            _logger = logger;
        }

        private void OnEnable()
        {

            _playerStatePanel = GetComponent<UIDocument>().rootVisualElement;
            _moneyLabel = _playerStatePanel.Q<Label>("MoneyValue");
            _logList = _playerStatePanel.Q<ListView>("LogList");

            _playerStatePanel.style.display = DisplayStyle.None;
            _logger.CardLog += AddLog;

            _logList.makeItem = MakeItem;
            _logList.bindItem = BindItem;
            _logList.itemsSource = _logs;

            _upgradeButton = _playerStatePanel.Q<Button>("UpgradeButton");
            _upgradeButton.clicked += ShowBuildingPanel;
        }

        public void Init(NetworkPlayer networkPlayer)
        {
            _playerStatePanel.style.display = DisplayStyle.Flex;
            _networkPlayer = networkPlayer;
        }

        public void ShowBuildingPanel()
        {
            _buildingPanelView.Show();
        }

        public void Update()
        {
            if (_playerStatePanel.style.display == DisplayStyle.None) return;

            _moneyLabel.text = _playerStateService.GetMoney(_networkPlayer.Player).ToString();
        }

        private VisualElement MakeItem()
        {
            VisualElement listItem = LogAsset.CloneTree();
            return listItem;
        }

        private void BindItem(VisualElement e, int index)
        {
            e.Q<Label>("CardLog").text = $"{(PlayerType)_logs[index].Player} => {(CardType)_logs[index].CardType}";
        }

        public void AddLog(Log log)
        {
            _logs.Add(log);
            _logList.Rebuild();
            _logList.ScrollToItem(_logs.Count - 1);
        }
    }
}
