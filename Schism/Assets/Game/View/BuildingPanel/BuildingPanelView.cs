﻿using System;
using System.Collections.Generic;
using Assets.Game.Constants;
using Assets.Game.Factories.Buildings;
using Assets.Game.Factories.Units;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Assets.Game.View.BuildingPanel
{
    public class BuildingPanelView : MonoBehaviour
    {
        public VisualTreeAsset BuildingButton;
        public event Action<BuildingType> Build;
        public event Action Cancel;

        private VisualElement _buildingPanel;
        private List<VisualElement> _buttons;
        private Button _cancelButton;

        private List<BuildingType> _buildingList = new List<BuildingType>
        {
            BuildingType.Citadel1,
            BuildingType.Crypt1
        };

        private IBuildingSoFactory _buildingSoFactory;

        [Inject]
        private void Construct(IBuildingSoFactory buildingSoFactory)
        {
            _buildingSoFactory = buildingSoFactory;
        }

        private void OnEnable()
        {
            _buildingPanel = GetComponent<UIDocument>().rootVisualElement.Q("BuildingPanel");
            _buildingPanel.style.display = DisplayStyle.None;

            _cancelButton = _buildingPanel.Q<Button>("CancelButton");
            _cancelButton.clicked += () =>
            {
                Cancel?.Invoke();
                Hide();
            };
        }

        public void Show()
        {
            _buildingPanel.style.display = DisplayStyle.Flex;
            ShowBuildingsButtons();
        }

        private void ShowBuildingsButtons()
        {
            var buttons = _buildingPanel.Q<VisualElement>("Buttons");
            _buttons?.ForEach(x => buttons.Remove(x));

            _buttons = new List<VisualElement>();

            
            foreach (var building in _buildingList)
            {
                var buildingSo = _buildingSoFactory.Create(building);
                var buildingSprite = buildingSo.Image;
                var buttonContainer = BuildingButton.CloneTree();
                var button = buttonContainer.Q<Button>("CreateBuilding");
                button.style.backgroundImage = new StyleBackground(buildingSprite);

                button.clicked += () => {
                    Build?.Invoke(building);

                    int index;
                    switch (building)
                    {
                        case BuildingType.Citadel1:
                            index = _buildingList.IndexOf(BuildingType.Citadel1);
                            _buildingList[index] = BuildingType.Citadel2;
                            break;
                        case BuildingType.Citadel2:
                            index = _buildingList.IndexOf(BuildingType.Citadel2);
                            _buildingList[index] = BuildingType.Citadel3;
                            break;
                        case BuildingType.Citadel3:
                            index = _buildingList.IndexOf(BuildingType.Citadel3);
                            _buildingList[index] = BuildingType.Citadel4;
                            break;
                        case BuildingType.Citadel4:
                            _buildingList.Remove(BuildingType.Citadel4);
                            break;
                        case BuildingType.Crypt1:
                            index = _buildingList.IndexOf(BuildingType.Crypt1);
                            _buildingList[index] = BuildingType.Crypt2;
                            break;
                        case BuildingType.Crypt2:
                            _buildingList.Remove(BuildingType.Crypt2);
                            break;
                    }

                    Hide();
                };

                buttons.Add(buttonContainer);
                _buttons.Add(buttonContainer);
            }
        }

        public void Hide()
        {
            _buildingPanel.style.display = DisplayStyle.None;
        }
    }
}
