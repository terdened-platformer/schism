﻿using System.Collections.Generic;
using Assets.Game.Constants;
using Assets.Game.Controllers.Buildings;
using Assets.Game.Controllers.Common;
using Assets.Game.Controllers.Utils;
using Assets.Game.Factories.Units;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Assets.Game.View.BarrackPanel
{
    public class BarrackPanelView : MonoBehaviour
    {
        public VisualTreeAsset ReplaceButton;

        private VisualElement _barrackPanel;
        private ProgressBar _healthBar;
        private Barrack _barrack;
        private List<VisualElement> _buttons;
        private IUnitFactory _unitFactory;

        [Inject]
        private void Construct(IUnitFactory unitFactory)
        {
            _unitFactory = unitFactory;
        }

        private void OnEnable()
        {
            _barrackPanel = GetComponent<UIDocument>().rootVisualElement;
            _healthBar = _barrackPanel.Q<ProgressBar>("HealthBar");
            _barrackPanel.style.display = DisplayStyle.None;
        }

        public void Show(Barrack barrack, bool isOwn)
        {
            Hide();

            _barrackPanel.style.display = DisplayStyle.Flex;
            _barrack = barrack;

            _barrack.GetComponent<SelectableObject>().Select();

            ShowReplaceUnitButtons(barrack, isOwn);
        }

        private void ShowReplaceUnitButtons(Barrack barrack, bool isOwn)
        {
            var buttons = _barrackPanel.Q<VisualElement>("Buttons");
            _buttons?.ForEach(x => buttons.Remove(x));

            _buttons = new List<VisualElement>();

            foreach (var unit in barrack.UnitWaveNet)
            {
                var unitSprite = _unitFactory.GetAvatar((UnitType) unit);
                var buttonContainer = ReplaceButton.CloneTree();
                var button = buttonContainer.Q<VisualElement>("ReplaceUnit");
                button.style.backgroundImage = new StyleBackground(unitSprite);
                buttons.Add(buttonContainer);
                _buttons.Add(buttonContainer);
            }
        }

        public void Hide()
        {
            if (_barrack == null)
                return;

            _barrackPanel.style.display = DisplayStyle.None;
            _barrack.GetComponent<SelectableObject>().Unselect();
            _barrack = null;
        }

        public void Update()
        {
            if (_barrackPanel.style.display == DisplayStyle.None || _barrack == null) return;

            var endurance = _barrack.GetComponent<HealthPower>();
            _healthBar.lowValue = endurance.Health.Value;
            _healthBar.highValue = endurance.MaxHealth.Value;
            _healthBar.title = $"{endurance.Health.Value}/{endurance.MaxHealth.Value}";
        }
    }
}
