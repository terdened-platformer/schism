﻿using System;
using System.Collections.Generic;
using Assets.Game.Constants;
using Assets.Game.Controllers.Buildings;
using Assets.Game.Factories.Units;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Assets.Game.View.ReplaceUnitPanel
{
    public class ReplaceUnitPanelView : MonoBehaviour
    {
        public VisualTreeAsset ReplaceButton;
        public event Action<Barrack, UnitType, int> ReplaceUnit;
        public event Action Cancel;

        private VisualElement _replaceUnitPanel;
        private Barrack _barrack;
        private UnitType _type;
        private List<VisualElement> _buttons;
        private Button _cancelButton; 
        private IUnitFactory _unitFactory;

        [Inject]
        private void Construct(IUnitFactory unitFactory)
        {
            _unitFactory = unitFactory;
        }

        private void OnEnable()
        {
            _replaceUnitPanel = GetComponent<UIDocument>().rootVisualElement.Q("ReplaceUnitPanel");
            _replaceUnitPanel.style.display = DisplayStyle.None;

            _cancelButton = _replaceUnitPanel.Q<Button>("CancelButton");
            _cancelButton.clicked += () =>
            {
                Cancel?.Invoke();
                Hide();
            };
        }

        public void Show(Barrack barrack, UnitType type)
        {
            _type = type;
            _replaceUnitPanel.style.display = DisplayStyle.Flex;
            _barrack = barrack;

            ShowReplaceUnitButtons(barrack);
        }

        private void ShowReplaceUnitButtons(Barrack barrack)
        {
            var buttons = _replaceUnitPanel.Q<VisualElement>("Buttons");
            _buttons?.ForEach(x => buttons.Remove(x));

            _buttons = new List<VisualElement>();


            var i = 0;
            foreach (var unit in barrack.UnitWaveNet)
            {
                var nonCapturedIndex = i;
                var unitSprite = _unitFactory.GetAvatar((UnitType)unit);
                var buttonContainer = ReplaceButton.CloneTree();
                var button = buttonContainer.Q<Button>("ReplaceUnit");
                button.style.backgroundImage = new StyleBackground(unitSprite);

                button.clicked += () => { 
                    ReplaceUnit?.Invoke(_barrack, _type, nonCapturedIndex);
                    Hide();
                };

                buttons.Add(buttonContainer);
                _buttons.Add(buttonContainer);
                i++;
            }
        }

        public void Hide()
        {
            if (_barrack == null)
                return;

            _replaceUnitPanel.style.display = DisplayStyle.None;
            _barrack = null;
        }
    }
}
