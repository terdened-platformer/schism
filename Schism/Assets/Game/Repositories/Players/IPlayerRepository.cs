﻿using System.Collections.Generic;
using Assets.Common.Constants;
using Assets.Game.Models.Player;

namespace Assets.Game.Repositories.Players
{
    public interface IPlayerRepository<T> where T: IPlayer
    {
        IEnumerable<T> Get();

        T GetByPlayer(PlayerType player);
    }
}