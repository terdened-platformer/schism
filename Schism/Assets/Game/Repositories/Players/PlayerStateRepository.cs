﻿using System.Collections.Generic;
using System.Linq;
using Assets.Common.Constants;
using Assets.Game.Models.Player;
using UnityEngine;

namespace Assets.Game.Repositories.Players
{
    public class PlayerStateRepository: IPlayerRepository<PlayerState>
    {
        public IEnumerable<PlayerState> Get()
        {
            var objs = Object.FindObjectsOfType<PlayerState>();
            return objs;
        }

        public PlayerState GetByPlayer(PlayerType player)
        {
            return Get().FirstOrDefault(_ => _.Player == player);
        }
    }
}
