﻿using Assets.Game.Controllers.Cards;
using Assets.Game.Controllers.Game;
using Assets.Game.Controllers.Players;
using Assets.Game.Factories.Buildings;
using Assets.Game.Factories.Cards;
using Assets.Game.Factories.Units;
using Assets.Game.Models.Player;
using Assets.Game.Repositories.Players;
using Assets.Game.Services.Players;
using Assets.Game.View.BarrackPanel;
using Assets.Game.View.BuildingPanel;
using Assets.Game.View.PlayerStatePanel;
using Assets.Game.View.ReplaceUnitPanel;
using UnityEngine;
using Zenject;
using Logger = Assets.Game.Controllers.Utils.Logger;

namespace Assets.Game.Infrastructure
{
    public class GameInstaller : MonoInstaller, IInitializable
    {
        public GameObject BuildingPanel;
        public GameObject ReplaceUnitPanel;
        public GameObject BarrackPanel;
        public GameObject PlayerStatePanel;
        
        public void Initialize()
        {
            var cardFactory = Container.Resolve<ICardFactory>();
            cardFactory.Load();

            var cardSoFactory = Container.Resolve<ICardSoFactory>();
            cardSoFactory.Load();
            
            var buildingSoFactory = Container.Resolve<IBuildingSoFactory>();
            buildingSoFactory.Load();

            var unitFactory = Container.Resolve<IUnitFactory>();
            unitFactory.Load();
        }

        public override void InstallBindings()
        {
            BindInstallerInterfaces();

            // Factories
            BindCardFactory();
            BindCardSoFactory();
            BindUnitFactory();
            BindBuildingSoFactory();

            // Repositories
            BindPlayerStateRepository();

            // Services
            BindPlayerStateService();

            // GameObjects
            BindPlayerCardsController();
            BindGameManager();
            BindDeckController();
            BindHandController();
            BindLogger();

            // UI
            BindBuildingPanel();
            BindReplaceUnitPanel();
            BindBarrackPanel();
            BindPlayerStatePanel();
        }

        private void BindInstallerInterfaces()
        {
            Container
                .BindInterfacesTo<GameInstaller>()
                .FromInstance(this)
                .AsSingle();
        }

        private void BindPlayerCardsController()
        {
            var playerCardController = FindObjectOfType<PlayerCardHandler>();

            Container
                .Bind<PlayerCardHandler>()
                .FromInstance(playerCardController)
                .AsSingle();
        }

        private void BindGameManager()
        {
            var gameManager = FindObjectOfType<GameManager>();

            Container
                .Bind<GameManager>()
                .FromInstance(gameManager)
                .AsSingle();
        }

        private void BindLogger()
        {
            var logger = FindObjectOfType<Logger>();

            Container
                .Bind<Logger>()
                .FromInstance(logger)
                .AsSingle();
        }

        private void BindDeckController()
        {
            var deckController = FindObjectOfType<DeckController>();

            Container
                .Bind<DeckController>()
                .FromInstance(deckController)
                .AsSingle();
        }
        
        private void BindHandController()
        {
            var handController = FindObjectOfType<HandController>();

            Container
                .Bind<HandController>()
                .FromInstance(handController)
                .AsSingle();
        }

        private void BindCardFactory()
        {
            Container
                .Bind<ICardFactory>()
                .To<CardFactory>()
                .AsSingle();
        }

        private void BindCardSoFactory()
        {
            Container
                .Bind<ICardSoFactory>()
                .To<CardSoFactory>()
                .AsSingle();
        }

        private void BindUnitFactory()
        {
            Container
                .Bind<IUnitFactory>()
                .To<UnitFactory>()
                .AsSingle();
        }
        
        private void BindBuildingSoFactory()
        {
            Container
                .Bind<IBuildingSoFactory>()
                .To<BuildingSoFactory>()
                .AsSingle();
        }

        private void BindPlayerStateRepository()
        {
            Container
                .Bind<IPlayerRepository<PlayerState>>()
                .To<PlayerStateRepository>()
                .AsSingle();
        }

        private void BindPlayerStateService()
        {
            Container
                .Bind<IPlayerStateService>()
                .To<PlayerStateService>()
                .AsSingle();
        }

        private void BindReplaceUnitPanel()
        {
            var replaceUnitPanelController = Container
                .InstantiatePrefabForComponent<ReplaceUnitPanelView>(ReplaceUnitPanel);

            Container
                .Bind<ReplaceUnitPanelView>()
                .FromInstance(replaceUnitPanelController)
                .AsSingle();
        }

        private void BindBuildingPanel()
        {
            var buildingPanelView = Container
                .InstantiatePrefabForComponent<BuildingPanelView>(BuildingPanel);

            Container
                .Bind<BuildingPanelView>()
                .FromInstance(buildingPanelView)
                .AsSingle();
        }

        private void BindBarrackPanel()
        {
            var barrackPanelController = Container
                .InstantiatePrefabForComponent<BarrackPanelView>(BarrackPanel);

            Container
                .Bind<BarrackPanelView>()
                .FromInstance(barrackPanelController)
                .AsSingle();
        }

        private void BindPlayerStatePanel()
        {
            var playerStatePanelController = Container
                .InstantiatePrefabForComponent<PlayerStatePanelView>(PlayerStatePanel);

            Container
                .Bind<PlayerStatePanelView>()
                .FromInstance(playerStatePanelController)
                .AsSingle();
        }
    }
}
