﻿using Assets.MainMenu.Ui.DecksPanel;
using Assets.MainMenu.Ui.DecksPanel.DeckPanel;
using Assets.MainMenu.Ui.MainMenu;
using Assets.MainMenu.Ui.QuickGamePanel;
using UnityEngine;
using Zenject;

namespace Assets.MainMenu.Infrastructure
{
    public class MainMenuInstaller : MonoInstaller
    {
        public GameObject DecksPanelPrefab;
        public GameObject DeckPanelPrefab;
        public GameObject QuickGamePanelPrefab;
        public GameObject MainMenuPanelPrefab;

        public override void InstallBindings()
        {
            BindMainMenuPanel();
            BindDecksPanel();
            BindDeckPanel();
            BindQuickGamePanel();
        }

        private void BindMainMenuPanel()
        {
            var mainMenuPanelController = Container
                .InstantiatePrefabForComponent<MainMenuPanelController>(MainMenuPanelPrefab);

            Container
                .Bind<MainMenuPanelController>()
                .FromInstance(mainMenuPanelController)
                .AsSingle();
        }

        private void BindQuickGamePanel()
        {
            var quickGamePanelController = Container
                .InstantiatePrefabForComponent<QuickGamePanelController>(QuickGamePanelPrefab);

            Container
                .Bind<QuickGamePanelController>()
                .FromInstance(quickGamePanelController)
                .AsSingle();
        }

        private void BindDeckPanel()
        {
            var deckPanelController = Container
                .InstantiatePrefabForComponent<DeckPanelController>(DeckPanelPrefab);

            Container
                .Bind<DeckPanelController>()
                .FromInstance(deckPanelController)
                .AsSingle();
        }

        private void BindDecksPanel()
        {
            var decksPanelController = Container
                .InstantiatePrefabForComponent<DecksPanelController>(DecksPanelPrefab);

            Container
                .Bind<DecksPanelController>()
                .FromInstance(decksPanelController)
                .AsSingle();
        }
    }
}
