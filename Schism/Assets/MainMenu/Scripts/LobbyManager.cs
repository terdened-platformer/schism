﻿using Assets.Common.Models;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.MainMenu.Scripts
{
    public class LobbyManager: MonoBehaviour
    {
        public Button ConnectButton;
        public Button CreateServerButton;
        public Dropdown LevelSelect;

        private void Start()
        {
            var btn = ConnectButton.GetComponent<Button>();
            btn.onClick.AddListener(OnConnectButtonClick);

            var btn1 = CreateServerButton.GetComponent<Button>();
            btn1.onClick.AddListener(OnCreateServerButtonClick);
        }

        private void OnConnectButtonClick()
        {
            Global.IsServer = false;
            Global.IsDebug = false;
            SceneManager.LoadScene(GetLevelName());
        }

        private void OnCreateServerButtonClick()
        {
            Global.IsServer = true;
            Global.IsDebug = false;
            SceneManager.LoadScene(GetLevelName());
        }

        private string GetLevelName()
        {
            return LevelSelect.value == 0 ? "Game2Player" : "Game4Player";
        }
    }
}
