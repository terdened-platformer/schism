using Assets.Common.Models;
using Assets.MainMenu.Ui.DecksPanel;
using Assets.MainMenu.Ui.DecksPanel.DeckPanel;
using Assets.MainMenu.Ui.MainMenu;
using Assets.MainMenu.Ui.QuickGamePanel;
using UnityEngine;
using Zenject;

namespace Assets.MainMenu.Ui
{
    public class MainMenuScreen : MonoBehaviour
    {
        private MainMenuPanelController _mainMenuPanelController;
        private DecksPanelController _decksPanelController;
        private QuickGamePanelController _quickGamePanelController;
        private DeckPanelController _deckPanelController;

        [Inject]
        private void Construct(DecksPanelController decksPanelController,
            MainMenuPanelController mainMenuPanelController,
            QuickGamePanelController quickGamePanelController,
            DeckPanelController deckPanelController)
        {
            _mainMenuPanelController = mainMenuPanelController;
            _mainMenuPanelController.DeckButtonClickEvent += ShowDecksPanel;
            _mainMenuPanelController.QuickGameButtonClickEvent += ShowQuickGamePanel;

            _quickGamePanelController = quickGamePanelController;
            _quickGamePanelController.BackButtonClickEvent += ShowMainMenuPanel;

            _deckPanelController = deckPanelController;
            _deckPanelController.BackButtonClickEvent += ShowDecksPanel;

            _decksPanelController = decksPanelController;

            _decksPanelController.BackButtonClickEvent += ShowMainMenuPanel;
            _decksPanelController.DeckButtonClickEvent += ShowDeckPanel;
        }

        private void ShowDecksPanel()
        {
            _deckPanelController.Hide();
            _mainMenuPanelController.Hide();
            _decksPanelController.Show();
        }

        private void ShowQuickGamePanel()
        {
            _mainMenuPanelController.Hide();
            _quickGamePanelController.Show();
        }

        private void ShowMainMenuPanel()
        {
            _quickGamePanelController.Hide();
            _decksPanelController.Hide();
            _mainMenuPanelController.Show();
        }

        private void ShowDeckPanel(Deck deck)
        {
            _decksPanelController.Hide();
            _deckPanelController.Show(deck);
        }
    }
}

