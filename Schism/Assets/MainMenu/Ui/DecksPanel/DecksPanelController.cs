﻿using System;
using System.Collections.Generic;
using Assets.Common.Models;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.MainMenu.Ui.DecksPanel
{
    public class DecksPanelController : MonoBehaviour
    {
        public VisualTreeAsset DeckButton;
        public event Action BackButtonClickEvent;
        public event Action<Deck> DeckButtonClickEvent;

        private VisualElement _decksPanel;
        private List<VisualElement> _buttons;

        private void OnEnable()
        {
            _decksPanel = GetComponent<UIDocument>().rootVisualElement;
            _decksPanel.style.display = DisplayStyle.None;

            var backButton = _decksPanel.Q<Button>("Back");
            backButton.clicked += () => {
                BackButtonClickEvent?.Invoke();
            };

            var newDeckButton = _decksPanel.Q<Button>("NewDeck");
            newDeckButton.clicked += () => {
                DeckButtonClickEvent?.Invoke(null);
            };
        }

        public void Show()
        {
            _decksPanel.style.display = DisplayStyle.Flex;

            var decksContainer = _decksPanel.Q<VisualElement>("DecksContainer");
            _buttons?.ForEach(x => decksContainer.Remove(x));

            _buttons = new List<VisualElement>();

            foreach (var deck in Global.Decks.Decks)
            {
                var buttonContainer = DeckButton.CloneTree();
                var button = buttonContainer.Q<Button>("DeckButton");

                button.text = deck.Title;
                button.clicked += () => {
                    DeckButtonClickEvent?.Invoke(deck);
                };

                decksContainer.Add(buttonContainer);
                _buttons.Add(buttonContainer);
            }
        }

        public void Hide()
        {
            _decksPanel.style.display = DisplayStyle.None;
        }
    }
}
