﻿using System;
using System.Collections.Generic;
using Assets.Common.Constants;
using Assets.Common.Models;
using Assets.Game.Constants;
using Assets.Game.Models.Cards;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.MainMenu.Ui.DecksPanel.DeckPanel
{
    public class DeckPanelController : MonoBehaviour
    {
        public List<CardModel> Cards;
        public VisualTreeAsset CardPanel;
        
        public event Action BackButtonClickEvent;

        private VisualElement _deckPanel;
        private Dictionary<CardType, VisualElement> _cards;
        private Deck _deck;

        void OnEnable()
        {
            _deckPanel = GetComponent<UIDocument>().rootVisualElement;
            _deckPanel.style.display = DisplayStyle.None;

            var backButton = _deckPanel.Q<Button>("Back");
            backButton.clicked += () => {
                BackButtonClickEvent?.Invoke();
            };

            var saveButton = _deckPanel.Q<Button>("Save");
            saveButton.clicked += Save;

            var deleteButton = _deckPanel.Q<Button>("Delete");
            deleteButton.clicked += Delete;
        }

        public void Show(Deck deck)
        {
            _deck = deck; 
            _deckPanel.style.display = DisplayStyle.Flex;

            var titleInput = _deckPanel.Q<TextField>("DeckTitle");
            titleInput.value = deck?.Title ?? "New Deck";

            var cardsContainer = _deckPanel.Q<VisualElement>("CardsContainer");

            if(_cards != null)
            {
                foreach (var card in _cards)
                {
                    cardsContainer.Remove(card.Value);
                }
            }

            _cards = new Dictionary<CardType, VisualElement>();

            foreach (var card in Cards)
            {
                var cardPanelContainer = CardPanel.CloneTree();

                var cardTitle = cardPanelContainer.Q<Label>("CardTitle");
                cardTitle.text = card.Title;

                var cardDescription = cardPanelContainer.Q<Label>("CardDescription");
                cardDescription.text = card.Description;

                var cardSprite = card.Image;
                var cardImage = cardPanelContainer.Q<VisualElement>("CardImage");
                cardImage.style.backgroundImage = new StyleBackground(cardSprite);

                var isSelected = cardPanelContainer.Q<Toggle>("IsSelected");
                isSelected.value = deck != null && deck.Cards.Contains(card.Type);

                cardsContainer.Add(cardPanelContainer);
                _cards.Add(card.Type, cardPanelContainer);
            }
        }

        private void Save()
        {
            if(_deck == null)
            {
                _deck = new Deck();
                Global.Decks.Decks.Add(_deck);
            }

            _deck.Cards = new List<CardType>();
            
            var titleInput = _deckPanel.Q<TextField>("DeckTitle");
            _deck.Title = titleInput.value;

            foreach (var (key, value) in _cards)
            {
                var isSelected = value.Q<Toggle>("IsSelected");

                if (isSelected.value)
                {
                    _deck.Cards.Add(key);
                }
            }

            Global.SaveDecks();
            BackButtonClickEvent?.Invoke();
        }

        public void Delete()
        {
            if(_deck != null)
            {
                Global.Decks.Decks.Remove(_deck);
                Global.SaveDecks();
            }

            BackButtonClickEvent?.Invoke();
        }

        public void Hide()
        {
            _deckPanel.style.display = DisplayStyle.None;
        }
    }
}
