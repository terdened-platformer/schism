﻿using System;
using System.Collections.Generic;
using Assets.Common.Models;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Assets.MainMenu.Ui.QuickGamePanel
{
    public class QuickGamePanelController : MonoBehaviour
    {
        public VisualTreeAsset DeckButton;
        public event Action BackButtonClickEvent;

        private VisualElement _quickGamePanel;
        private List<VisualElement> _buttons;

        private void OnEnable()
        {
            _quickGamePanel = GetComponent<UIDocument>().rootVisualElement;
            _quickGamePanel.style.display = DisplayStyle.None;

            var backButton = _quickGamePanel.Q<Button>("Back");
            backButton.clicked += () => {
                BackButtonClickEvent?.Invoke();
            };
        }

        public void Show()
        {
            _quickGamePanel.style.display = DisplayStyle.Flex;

            var decksContainer = _quickGamePanel.Q<VisualElement>("DecksContainer");
            _buttons?.ForEach(x => decksContainer.Remove(x));

            _buttons = new List<VisualElement>();

            foreach (var deck in Global.Decks.Decks)
            {
                var buttonContainer = DeckButton.CloneTree();
                var button = buttonContainer.Q<Button>("DeckButton");

                button.text = deck.Title;
                button.clicked += () => {
                    Global.SelectedDeck = deck;
                    Global.IsServer = false;
                    Global.IsDebug = false;
                    SceneManager.LoadScene("Game2Player");
                };

                decksContainer.Add(buttonContainer);
                _buttons.Add(buttonContainer);
            }

            _quickGamePanel.style.display = DisplayStyle.Flex;
        }

        public void Hide()
        {
            _quickGamePanel.style.display = DisplayStyle.None;
        }
    }
}
