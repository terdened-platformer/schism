﻿using System;
using Assets.Common.Models;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Assets.MainMenu.Ui.MainMenu
{
    public class MainMenuPanelController : MonoBehaviour
    {
        public event Action DeckButtonClickEvent;
        public event Action QuickGameButtonClickEvent;

        private VisualElement _mainMenuPanel;

        private void OnEnable()
        {
            _mainMenuPanel = GetComponent<UIDocument>().rootVisualElement;

            var quitButton = _mainMenuPanel.Q<Button>("Quit");
            quitButton.clicked += Application.Quit;

            var decksButton = _mainMenuPanel.Q<Button>("Decks");
            decksButton.clicked += () => {
                DeckButtonClickEvent?.Invoke();
            };

            var quickGameButton = _mainMenuPanel.Q<Button>("QuickGame");
            quickGameButton.clicked += () => {
                QuickGameButtonClickEvent?.Invoke();
            };

            var createServer = _mainMenuPanel.Q<Button>("CreateServer");
            createServer.clicked += () => {
                Global.IsServer = true;
                Global.IsDebug = false;
                SceneManager.LoadScene("Game2Player");
            };
        }

        public void Show()
        {
            _mainMenuPanel.style.display = DisplayStyle.Flex;
        }

        public void Hide()
        {
            _mainMenuPanel.style.display = DisplayStyle.None;
        }
    }
}
